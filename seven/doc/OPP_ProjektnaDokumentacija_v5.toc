\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{2}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{4}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{12}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{12}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{17}{subsection.3.1.1}% 
\contentsline {subsubsection}{Opis obrazaca uporabe}{17}{subsubsection*.2}% 
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{34}{subsubsection*.3}% 
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{37}{subsection.3.1.2}% 
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{41}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{43}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Baza podataka}{48}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{48}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{56}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Dijagram razreda}{57}{section.4.2}% 
\contentsline {chapter}{\numberline {5}Zaključak i budući rad}{63}{chapter.5}% 
\contentsline {chapter}{Popis literature}{65}{chapter*.4}% 
\contentsline {chapter}{Indeks slika i dijagrama}{66}{chapter*.5}% 
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{67}{chapter*.6}% 
