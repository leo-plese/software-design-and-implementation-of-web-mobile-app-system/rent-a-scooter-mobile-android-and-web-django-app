"""scooters URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, re_path
from container.views import *
from django.contrib import admin
from django.urls import path


urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'register/', Register.as_view(), name="register"),
#    path(r'register_photos/', RegisterPhotos.as_view()),
    path(r'login/', Login.as_view()),
    path(r'delete_user/', DeleteUser.as_view(), name="delete_user"),
    path(r'delete_sooter/', DeleteScooter.as_view()),
    path(r'create_scooter/', CreateScooter.as_view(), name="create_scooter"),
    path(r'create_rentOffer/', CreateRentOffer.as_view()),
    path(r'update_info/', UpdateInfo.as_view()),
    path(r'reset_password/', PasswordReset.as_view()),
    path(r'set_password/', PasswordSet.as_view()),
    re_path(r'register/(?P<pk>\d+)', UserView.as_view()),
    path(r'account_data/', AccountData.as_view()),
    path(r'get_transaction/', GetTransactions.as_view()),
    path(r'create_transaction/', CreateTransaction.as_view()),
#    path(r'account_data_visibility/', DataVisibility.as_view()),
#    path(r'change_account_data_visibility/', ChangeDataVisibility.as_view()),
    path(r'create_rating/', CreateRating.as_view()),
    path(r'public_data/', PublicData.as_view()),
    path(r'delete_scooter/', DeleteScooter.as_view()),
    path(r'get_scooters/', GetScooterList.as_view()),
    path(r'change_roles/', ChangeRoles.as_view()),
    path(r'add_scooter_photo/', AddScooterPhoto.as_view()),
    path(r'delete_scooter_photo/', DeleteScooterPhoto.as_view()),
    path(r'get_rent_offers_renter/', GetRenterOffers.as_view()),
    path(r'get_rent_offers_client/', GetRenterOffersClient.as_view()),
    path(r'get_client_requests/', GetClientRequests.as_view()),
    path(r'get_active_offers/', GetActiveOffers.as_view()),
    path(r'send_request/', SendRequestForOffer.as_view()),
    path(r'confirm_client_request/', ConfirmClientRequestForOffer.as_view()),
    path(r'send_report/', SendReportForPhoto.as_view()),
    path(r'get_client_reports/', GetClientReports.as_view()),
    path(r'answer_client_report/', AnswerClientReport.as_view()),
    path(r'get_chats/', ShowChats.as_view()),
    path(r'get_chat_messages/', ShowMessages.as_view()),
    path(r'send_message/', SendMessage.as_view()),
    path(r'confirm_transaction/', ConfirmTransaction.as_view()),
    path(r'rentofferId/', GetRentOfferId.as_view()),
    path(r'get_offer_details/', GetOfferDetails.as_view()),
]
