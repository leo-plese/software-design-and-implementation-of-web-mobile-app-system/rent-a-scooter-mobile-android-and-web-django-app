from django.test import TestCase
from django.test import Client
from django.urls import reverse
from .models import *
from .serializer import *
import json
from rest_framework import status

client = Client()


class TestViews(TestCase):
    def setUp(self):
        self.register_valid_data = {
            'name': 'Rachel',
            'surname': 'Green',
            'nickname': 'Rach',
            'creditCardNumber': '12990483245',
            'email': 'rachel.green@friends.com',
            'password': 'notOnBrake',
            'idURL': 'illbethereforyouuuuuu',
            'recordURL': 'whentherainstartstopour',
            'isActive': True,
            'isClient': False,
            'isRenter': False,
        }
        # šaljemo bez maila
        self.register_invalid_data = {
            'name': 'Rachel',
            'surname': 'Green',
            'nickname': 'Rach',
            'creditCardNumber': '12990483245',
            'email': '',
            'password': 'notOnBrake',
            'idURL': 'illbethereforyouuuuuu',
            'recordURL': 'whentherainstartstopour',
            'isActive': True,
            'isClient': False,
            'isRenter': False,
        }

        self.monica = user.objects.create(
            name='Monica',
            surname='Geller',
            nickname='Mon',
            creditCardNumber='12990483244',
            email='monica.geller@friends.com',
            password='notOnBrake1',
            idURL='illbethereforyouuuuuu1',
            recordURL='whentherainstartstopour1',
            isActive=True,
            isClient=False,
            isRenter=False,
        )

        self.phoebesGrandma = user.objects.create(
            name='Grandma',
            surname='Buffay',
            nickname='Ma',
            creditCardNumber='1299048324413255',
            email='grandma@friends.com',
            password='freeRides',
            idURL='illbethereforyouuuuuu11',
            recordURL='whentherainstartstopour11',
            isActive=True,
            isClient=False,
            isRenter=False,
        )

        self.phoebesGrandmaTaxy = {
            'renterId' : self.phoebesGrandma.id,
            'comment':'Smelly cat...',
            'isAvailable' : True,
        }

        self.nobodysTaxy = {
            'renterId': '',
            'comment': 'Smelly cat...',
            'isAvailable': True,
        }


        self.delete_user_valid_data = {
            'email': 'monica.geller@friends.com',
        }

        #šaljemo email koji ne postoji u bazi ili prazan
        self.delete_user_invalid_data = {
            'email': 'ross.geller@friends.com',
        }

    def test_valid_registration(self):
        response = client.post(
            reverse('register'),
            data=json.dumps(self.register_valid_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_registration(self):
        response = client.post(
            reverse('register'),
            data=json.dumps(self.register_invalid_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_user(self):
        response = client.post(
            reverse('delete_user'),
            data=json.dumps(self.delete_user_valid_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_delete_user(self):
        response = client.post(
            reverse('delete_user'),
            data=json.dumps(self.delete_user_invalid_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_valid_create_scooter(self):
        response = client.post(
            reverse('create_scooter'),
            data=json.dumps(self.phoebesGrandmaTaxy),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_create_scooter(self):
        response = client.post(
            reverse('create_scooter'),
            data=json.dumps(self.nobodysTaxy),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
