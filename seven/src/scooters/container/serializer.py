from rest_framework import serializers
from .models import user, showUserData
from .models import user, transaction

from .models import *


class ScooterIdSerializer(serializers.ModelSerializer):
    # id = serializers.ReadOnlyField()
    class Meta:
        model = scooter
        fields = ['id']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'name',
            'surname',
            'nickname',
            'creditCardNumber',
            'email',
            'isClient',
            'isRenter',
            'idURL',
            'recordURL',
        ]


class UserSerializer2(serializers.ModelSerializer):
    class Meta:
        model = showUserData
        fields = [
            'showName',
            'showSurname',
            'showEmail',
        ]


class ScooterPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = scooterPhoto
        fields = [
            'scooterId',
            'imgURL'
        ]


class CreateScooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = scooter
        fields = [
            'renterId',
            'comment',
            'isAvailable',
        ]


class AddScooterPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = scooterPhoto
        fields = [
            'scooterId',
            'imgURL',
        ]


class GetScooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = scooter
        fields = [
            'id',
            'renterId',
            'comment',
            'isAvailable',
        ]


class DateTimeSerializer(serializers.ModelSerializer):
    dateTime = serializers.DateTimeField(allow_null=True)

    class Meta:
        model = rentOffer
        fields = [
            'dateTime',
        ]

class DeleteScooterSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=30, allow_null=True)

    class Meta:
        model = rentOffer
        fields = [
            'id', ]

class DeleteScooterPhotoSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=30, allow_null=True)

    class Meta:
        model = rentOffer
        fields = [
            'id', ]


class CreateRentOfferSerializer2(serializers.ModelSerializer):
    class Meta:
        model = rentOffer
        fields = [
            'scooterId',
        ]

class CreateRentOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = rentOffer
        fields = [
            'scooterId',
            'clientId',
            'currentLocationLatitude',
            'currentLocationLongitude',
            'returnLocationLatitude',
            'returnLocationLongitude',
            'returnTime',
            'priceKm',
            'returnPenalty',
        ]


class GetRentOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = rentOffer
        fields = [
            'id',
            'scooterId',
            'clientId',
            'currentLocationLatitude',
            'currentLocationLongitude',
            'returnLocationLatitude',
            'returnLocationLongitude',
            'returnTime',
            'priceKm',
            'returnPenalty',
        ]

class GetRentOfferSerializer2(serializers.ModelSerializer):
    class Meta:
        model = rentOffer
        fields = [
            'id',
            'scooterId',
            'currentLocationLatitude',
            'currentLocationLongitude',
            'returnLocationLatitude',
            'returnLocationLongitude',
            'returnTime',
            'priceKm',
            'returnPenalty',
        ]


class EmailSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField

    class Meta:
        model = user
        fields = [
            'email',
        ]

'''class EmailPhotoSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField

    class Meta:
        model = user
        fields = [
            'email',
            'idURL',
            'recordURL',
        ]'''


class RentOfferIdSerializer(serializers.ModelSerializer):
    id=serializers.CharField(max_length=30, allow_null=True)
    class Meta:
        model = rentOffer
        fields = [
            'id',
        ]


class ScooterIdSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField

    class Meta:
        model = scooter
        fields = [
            'id',
        ]

class WholeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'id',
            'name',
            'surname',
            'nickname',
            'creditCardNumber',
            'email',
            'password',
            'idURL',
            'recordURL',
            'isActive',
            'isClient',
            'isRenter',
        ]

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'email',
            'password',
        ]


class ResetPasswordSerializer(serializers.ModelSerializer):
    oldPassword = serializers.CharField(max_length=30, allow_blank=False)
    newPassword = serializers.CharField(max_length=30, allow_blank=False)

    '''def create(self, validated_data):
        validated_data.pop('oldPassword', None)
        validated_data.pop('newPassword', None)
        return super().create(validated_data)'''

    class Meta:
        model = user
        fields = [
            'email',
            'oldPassword',
            'newPassword',
        ]


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = transaction
        fields = [
            'rentOfferId',
            'distanceCovered',
            'price',
            'pentalty',
            'time',
            'ratingId',
        ]

class GetOfferDetailsSerializer(serializers.ModelSerializer):
    offer_Id=serializers.CharField(max_length=30, allow_null=True)
    class Meta:
        model = rentOffer
        fields = [
            'offer_Id',
        ]

class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = rating
        fields = [
            'clientId',
            'renterId',
            'rating',
            'comment',
        ]


class UpdateInfoSerializer(serializers.ModelSerializer):
    showName = serializers.BooleanField(allow_null=True)
    showSurname = serializers.BooleanField(allow_null=True)
    showEmail = serializers.BooleanField(allow_null=True)

    class Meta:
        model = user
        fields = [
            'name',
            'surname',
            'nickname',
            'creditCardNumber',
            'email',
            # 'password',
            'showName',
            'showSurname',
            'showEmail',
        ]


class PublicDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'nickname',
            'name',
            'surname',
            'email',
        ]


class PublicDataSerializer2(serializers.ModelSerializer):
    nickname = serializers.SerializerMethodField('getnick')

    def getnick(self, obj):
        return user.objects.filter(id=obj.renterId.id)[0].nickname

    class Meta:
        model = rating
        fields = [
            'nickname',
            'rating',
            'comment',
        ]


class PublicDataSerializer3(serializers.ModelSerializer):
    nickname = serializers.SerializerMethodField('getnick')

    def getnick(self, obj):
        return user.objects.filter(id=obj.clientId.id)[0].nickname

    class Meta:
        model = rating
        fields = [
            'nickname',
            'rating',
            'comment',
        ]


class DataVisibilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = showUserData
        fields = [
            'userId',
            'showName',
            'showSurname',
            'showEmail',
        ]


class GetTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = transaction
        fields = [
            'id',
            'distanceCovered',
            'price',
            'pentalty',
            'time',
        ]


class GetTransactionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = rentOffer
        fields = [
            'currentLocationLatitude',
            'currentLocationLongitude',
            'returnLocationLatitude',
            'returnLocationLongitude',
            'returnTime',
            'priceKm',
            'returnPenalty',
        ]


class GetTransactionSerializer3(serializers.ModelSerializer):
    class Meta:
        model = scooter
        fields = [
            'comment',
            # nekak dodati slike hahahahahaha
        ]


class GetTransactionSerializer4(serializers.ModelSerializer):
    class Meta:
        model = rating
        fields = [
            'rating',
            'comment',
        ]


class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'email',
            'isClient',
            'isRenter',
        ]


class UserClientRenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = [
            'name',
            'surname',
            'nickname',
            'creditCardNumber',
            'email',
            'isClient',
            'isRenter',
        ]


class SendRequestForOfferSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=30, allow_null=True, allow_blank=False)
    senderId = serializers.CharField(max_length=30, allow_null=True, allow_blank=False)
    text = serializers.CharField(max_length=300, allow_null=True, allow_blank=True)

    class Meta:
        model = rentOffer
        fields = [
            'id',
            'senderId',
            'text',
        ]


class TextTimestampSerializer(serializers.ModelSerializer):
    class Meta:
        model = message
        fields = [
            # 'text',
            'time',
        ]

class TimestampSerializer(serializers.ModelSerializer):
    class Meta:
        model = message
        fields = [
            'time',
        ]

class ClientRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = chat
        fields = [
            'clientId',
        ]


class ConfirmClientRequestForOfferSerializer(serializers.ModelSerializer):
    chatId = serializers.CharField(max_length=30, allow_null=True, allow_blank=False)
    rentOfferId = serializers.CharField(max_length=30, allow_null=True, allow_blank=False)
    class Meta:
        model = chat
        fields = [
            'chatId',
            'rentOfferId',
        ]

class ReportPhotoSerializer(serializers.ModelSerializer):
    cause = serializers.CharField(max_length=1000, allow_null=True, allow_blank=False)
    photosToAdd = serializers.CharField(max_length=10000, allow_null=True, allow_blank=False)
    photosToRemove = serializers.CharField(max_length=10000, allow_null=True, allow_blank=False)

    class Meta:
        model = reportPhoto
        fields = [
            'rentOfferId',
            'cause',
            'photosToAdd',
            'photosToRemove',
        ]
class ReportPhotoSerializer2(serializers.ModelSerializer):

    class Meta:
        model = reportPhoto
        fields = [
            'rentOfferId',
            'cause',
        ]
class AnswerReportPhotoSerializer(serializers.ModelSerializer):
    id=serializers.CharField(max_length=30, allow_null=True)
    class Meta:
        model = reportPhoto
        fields = [
            'id',
            'isAccepted',
        ]

class AnswerReportPhotoSerializer2(serializers.ModelSerializer):
    class Meta:
        model = reportPhoto
        fields = [
            'cause',
        ]
class ChatIdSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(allow_null=False)
    class Meta:
        model = chat
        fields = [
            'id',
        ]

class SendMessageSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(allow_null=False)
    text = serializers.CharField(allow_null=True, allow_blank=False)
    class Meta:
        model = chat
        fields = [
            'id',
            'text'
        ]

class CreateTransactionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(allow_null=False)
    class Meta:
        model = rentOffer
        fields = [
            'id',
        ]

class ConfirmTransactionSerializer(serializers.ModelSerializer):
    time = serializers.CharField(allow_null=False, allow_blank=False)
    class Meta:
        model = transaction
        fields = [
            'distanceCovered',
            'price',
            'pentalty',
            'time',
        ]

class CreateRatingSerializer(serializers.ModelSerializer):
    rentOfferId = serializers.CharField(allow_null=True, allow_blank=False)
    # rating = serializers.IntegerField(allow_null=False)
    # comment = serializers.CharField(allow_blank=False)#nakon promjene u models null=True postaviti na allow_blank=True
    class Meta:
        model = rating
        fields = [
            'rentOfferId',
            'rating',
            'comment',
        ]

class ScooterInfoForChatSerializer(serializers.ModelSerializer):
    # id = serializers.ReadOnlyField()
    class Meta:
        model = scooter
        fields = [
            'id',
            'comment'
        ]
