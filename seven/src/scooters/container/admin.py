import random
import string

from django.contrib import admin
from container.models import *
from django.core.mail import send_mail
from django.conf import settings


# Register your models here.

@admin.register(user)
class ModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'nickname', 'email', 'isActive', 'creditCardNumber','isClient', 'isRenter']
    list_filter = ['isActive']

    def save_model(self, request, obj, form, change):
        password = random_string(8)
        obj.password = password
        super(ModelAdmin, self).save_model(request, obj, form, change)
        if obj.isActive:
            send_mail('Scooters account password',
                      'Thank you for registration. \n\nYour Scooter account password is: ' + password +
                      '\nYou can set different password here: https://www.scooterssevenopp.com/set_password' +
                      '.\n\nThis is a no-reply email.',
                      settings.EMAIL_HOST_USER,
                      [obj.email],
                      fail_silently=False)

@admin.register(reportPhoto)
class ModelAdmin2(admin.ModelAdmin):
    list_display = ['rentOfferId', 'cause', 'isAccepted', 'isAcceptedByAdmin', 'oldPhotos','newPhotos']
    list_filter = ['isAccepted', 'isAcceptedByAdmin']

    def save_model(self, request, obj, form, change):
        super(ModelAdmin2, self).save_model(request, obj, form, change)
        if obj.isAcceptedByAdmin is True:
            tech=rentOffer.objects.get(id=obj.rentOfferId.id)
            scoot=scooter.objects.get(id=tech.scooterId.id)
            photlist=scooterPhoto.objects.filter(scooterId=scoot)
            for ph in photlist:
                #print('************************',ph.imgURL)
                ph.delete()
            if reportedPhotoAdd.objects.filter(reportPhotoId=obj.id):
                li = reportedPhotoAdd.objects.filter(reportPhotoId=obj.id)
                lis = []
                for l in li:
                    lis.append(l.newPhotoULR)
            #print(lis)
            for li in lis:
                u=scooterPhoto(scooterId=scoot, imgURL=li)
                u.save()
                #print(u, u.scooterId, u.imgURL)


    def newPhotos(self, obj):
        #print(obj.id)
        if reportedPhotoAdd.objects.filter(reportPhotoId=obj.id):
            li=reportedPhotoAdd.objects.filter(reportPhotoId=obj.id)
            ispis=[]
            for l in li:
                ispis.append(l.newPhotoULR)
            return ispis
        else: return None

    def oldPhotos(self, obj):
        #print(obj.id)
        if reportedPhotoRemove.objects.filter(reportPhotoId=obj.id):
            li=reportedPhotoRemove.objects.filter(reportPhotoId=obj.id)
            ispis=[]
            for l in li:
                ispis.append(l.oldPhotoULR)
            return ispis
        else: return None


    '''def save_model(self, request, obj, form, change):
        password = random_string(8)
        obj.password = password
        super(ModelAdmin, self).save_model(request, obj, form, change)
        if obj.isActive:
            send_mail('Scooters account password',
                      'Thank you for registration. \n\nYour Scooter account password is: ' + password +
                      '\nYou can set different password here: https://www.scooterssevenopp.com/set_password' +
                      '.\n\nThis is a no-reply email.',
                      settings.EMAIL_HOST_USER,
                      [obj.email],
                      fail_silently=False)'''


def random_string(string_length):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(string_length))

