package com.seven.iznajmiromobil.view.offers

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.OffersActivityBinding
import com.seven.iznajmiromobil.utils.FragmentViewPagerAdapter
import com.seven.iznajmiromobil.view.base.BaseActivity

class OffersActivity : BaseActivity() {

    private lateinit var pagerAdapter: FragmentViewPagerAdapter

    private lateinit var binding: OffersActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.offers_activity)
        binding.activity = this
        loadFragments()
    }

    private fun loadFragments() {
        pagerAdapter = FragmentViewPagerAdapter(supportFragmentManager, listOf(CurrentOffersFragment(), PastOffersFragment()))
        binding.viewPager.adapter = pagerAdapter
        binding.viewPager.offscreenPageLimit = 2
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.tabLayout.getTabAt(0)!!.text = this.getString(R.string.active)
        binding.tabLayout.getTabAt(1)!!.text = this.getString(R.string.past)
    }

    fun createNewOffer() {
        startActivity(Intent(this, CreateOfferActivity::class.java))
    }

}