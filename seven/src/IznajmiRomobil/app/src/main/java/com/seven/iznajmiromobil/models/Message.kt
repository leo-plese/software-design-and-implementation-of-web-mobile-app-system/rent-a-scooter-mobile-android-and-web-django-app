package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class Message {

    // acceptedRequest - flag set to true/false just for first message in chat (client request) at the moment when renter confirms one request, for all other messages other than client request message - null
    var acceptedRequest: Boolean? = null
    var messageNumber: Int = 0
    var text: String? = null
    var time: Timestamp = Timestamp(System.currentTimeMillis())
    var senderId: Int = 0
}