package com.seven.iznajmiromobil.network.api

import com.seven.iznajmiromobil.models.*
import io.reactivex.Observable
import retrofit2.http.*
import java.sql.Timestamp

interface UserApi {

    data class LoginRequestBody(var email: String, var password: String)
    data class ResetPasswordRequestBody(var email: String, var oldPassword: String, var newPassword: String)
    data class UserResponseBody(var user: User)

    data class AccountDataResponseBody(var user: User, var showUserData: ShowUserData)
    data class IDUserBody(var userId: Int)
    data class EmailUserBody(var email: String)
    data class NicknameUserBody(var nickname: String)
    data class UserAccountDataAndDataVisibilityBody(var name: String, var surname: String, var nickname: String, var email: String, var creditCardNumber: String, var showName: Boolean, var showSurname: Boolean, var showEmail: Boolean)
    //data class UserAccountDataVisibilityBody(var email: String, var showName: Boolean, var showSurname: Boolean, var showEmail: Boolean)
    data class ShowUserDataResponseBody(var showUserData: ShowUserData)

    data class TransactionResponseBody(var transactions: List<Transaction>)
    data class UserRolesBody(var email: String, var isRenter: Boolean, var isClient: Boolean)
    data class UserPublicProfileResponseBody(var userPublicProfile: UserPublicProfile)
    data class UserRatingsResponseBody(var userRatings: UserRatings)

    data class CurrentOffersResponseBody(var rentOffer: List<AdvertisedCurrentRentOffer>)
    data class SendMessageInChatRequestBody(
        var chatId: Int,    // user 1. chooses "My chats", 2. chooses one chat -> save chatId, 3. use that chatId to create/send new message for it HERE
        var text: String
    )
    data class SendMessageInChatResponseBody(
        var time: Timestamp    // just to give user a feedback about time when exactly the message was sent (time written in the message)
    )

    data class GetChatsResponseBody(
        var chats: List<Chat>
    )

    data class GetMessagesInChatRequestBody(
        var chatId: Int     // user 1. chooses "My chats", 2. chooses one chat -> save chatId, 3. use that chatId to get messages in that chat
    )
    data class GetMessagesInChatResponseBody(
        var messages: List<Message>
    )

    data class RegisterRequestBody(
        var name: String,
        var surname: String,
        var nickname: String,
        var creditCardNumber: String,
        var email: String,
        var isRenter: Boolean,
        var isClient: Boolean,
        var idURL: String,
        var recordURL: String
    )

    data class OffersResponseBody(var rentOffer: List<RenterRentOffer>)

    /**
     * UC 2
     * @param register user data
     * @return Unit (nothing to return)
     */
    @POST("register/")
    fun register(@Body registerBody: RegisterRequestBody): Observable<User>

    /**
     * UC 3
     * @param login user data
     * @return user data
     */
    @POST("login/")
    fun login(@Body loginBody: LoginRequestBody): Observable<UserResponseBody>

    /**
     * Called when user changes password.
     */
    @POST("reset_password/")
    fun resetPassword(@Body resetPasswordBody: ResetPasswordRequestBody): Observable<UserResponseBody>

    // called on deep link given from email
    @PUT("set_password/")
    fun setPassword(@Body resetPasswordBody: LoginRequestBody): Observable<UserResponseBody>


    /**
     * UC 4
     * @param user to delete - identified by email
     * @return Unit (nothing to return)
     */
    @DELETE("delete_user/")
    fun deleteUser(@Body deleteUserBody: EmailUserBody): Observable<Unit>

    /**
     * UC 5
     * @param user to get his account data - identified by email
     * @return User data
     */
    @POST("account_data/")
    fun getAccountData(@Body getAccountDataBody: EmailUserBody): Observable<AccountDataResponseBody>

    /**
     * UC 6
     * @param user data (name, surname, nickname, email, creditCardNumber) to update
     * @return User data
     */
    //@PUT("update_info/")
    //fun changeAccountData(@Body changeAccountDataBody: UserAccountDataBody): Observable<UserResponseBody>


    /**
     * @param user to get his account data - identified by email
     * @return User data visibility
     */
    /*
    @GET("account_data_visibility/")
    fun getAccountDataVisibility(@Body getAccountDataVisibilityBody: EmailUserBody): Observable<ShowUserDataResponseBody>
     */

    /**
     * UC 7
     * @param user data visibility (email, showName, showSurname, showEmail) to update
     * @return User data
     */
    //@PUT("change_account_data_visibility/")
    //fun changeAccountDataVisibility(@Body changeAccountDataVisibilityBody: UserAccountDataVisibilityBody): Observable<ShowUserDataResponseBody>

    /**
     * UC 6 + UC 7 merged into this one UC
     *
     * @param user data visibility (name, surname, nickname, email, creditCardNumber, showName, showSurname, showEmail) to update
     * @return User data
     */
    @POST("update_info/")
    fun changeAccountData(@Body changeAccountDataAndDataVisibilityBody: UserAccountDataAndDataVisibilityBody): Observable<UserResponseBody>


    /**
     * UC 8
     * @param user to get his account data - identified by email
     * @return User transactions data
     */
    @POST("get_transaction/")
    fun getUserTransactions(@Body getUserTransactionsBody: EmailUserBody): Observable<TransactionResponseBody>

    /**
     * UC 9, UC 10
     * @param user identified by email with new renter/client role
     * @return User data
     */
    @POST("change_roles/")
    fun changeUserRoles(@Body userRolesBody: UserRolesBody): Observable<UserResponseBody>


    /**
     * UC 11
     * @param user (any existing user) being searched in Search box - identified by email
     * @return User data
     */
    @POST("get_user_public_profile/")
    fun getUserPublicProfile(@Body nicknameUserBody: NicknameUserBody): Observable<UserPublicProfileResponseBody>

    /**
     *
     */
    @POST("get_user_ratings/")
    fun getUserRatings(@Body emailUserBody: EmailUserBody): Observable<UserRatingsResponseBody>


    /**
     * UC 1
     *
     * The first use case - any user (unregistered or registered (client or renter)) can do this - get current/actual/non-expired renter offers.
     *
     * @param nothing (get all the active i.e. current offers from all the renters)
     * @return current offers
     */
    @POST("get_active_offers/")
    fun getCurrentOffers(@Body timeStampBody: RenterApi.StringTimeStampBody): Observable<CurrentOffersResponseBody>

    /**
     * UC 22
     * @param chat to which message is being sent + message text
     * @return time when exactly the message was sent - little feedback to the user (could be nothing (Unit))
     */
    @POST("send_message/")
    fun sendMessageInChat(@Body sendMessageInChatRequestBody: SendMessageInChatRequestBody): Observable<SendMessageInChatResponseBody>

    /**
     * UC 23
     *
     * This method is called by user with renter role - can be ONLY renter or renter+client.
     *
     * @param chat to which message is being sent + message text
     * @return time when exactly the message was sent - little feedback to the user (could be nothing (Unit))
     */
    @POST("get_chats/")
    fun getChats(@Body emailUserBody: EmailUserBody): Observable<GetChatsResponseBody>


    /**
     * UC 24
     * @param chat whose messages are being retrieve
     * @return messages in chat
     */
    @POST("get_chat_messages/")
    fun getMessagesInChat(@Body getMessagesInChatRequestBody: GetMessagesInChatRequestBody): Observable<GetMessagesInChatResponseBody>


    data class OfferDetailsBody(var offer_Id: Int)

    data class OfferDetails(var rentOffer: RenterRentOffer, var scooter: Scooter, var renter: User, var client: List<User>)

    @POST("get_offer_details/")
    fun getOfferDetails(@Body offerIdBody: OfferDetailsBody): Observable<OfferDetails>

}