package com.seven.iznajmiromobil.view.registeredUser.mainActivities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.LinearLayout
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.RenterMainActivityBinding
import com.seven.iznajmiromobil.models.ClientReportsForReview
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.utils.TextUtils
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.UserPublicProfileActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.view.registeredUser.renter.ClientReportsActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class RenterMainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: RenterMainActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    private var searchUserNickname: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.renter_main_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // get stored user data
 //       val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
//        user  = Gson().fromJson(json, User::class.java)
    }

    override fun onBackPressed() {
        finishAffinity()
    }

    private fun subscribeUi() {
        userViewModel.getUserPublicProfileResult.observe(this, Observer { userPublicProfile ->
            if (userPublicProfile == null) {
                val nick = searchUserNickname
                searchUserNickname = null
                CommonFunctionsClass.showNoSearchedUserFoundPopup(this, nick)
            } else {
                val userPublicProfileActivityIntent = Intent(this, UserPublicProfileActivity::class.java)
                userPublicProfileActivityIntent.putExtra("userProfile", Gson().toJson(userPublicProfile))
                userPublicProfileActivityIntent.putExtra("sourceActivity", "RenterMainActivity")
                startActivity(userPublicProfileActivityIntent)
            }
        })

        userViewModel.getClientReportsResult.observe(this, Observer { clientReports ->
            val clientReportsActivityIntent = Intent(this, ClientReportsActivity::class.java)
            var clientReportsForReview = ClientReportsForReview()
            clientReportsForReview.clientReports = clientReports
            clientReportsActivityIntent.putExtra("clientReports", Gson().toJson(clientReportsForReview))
            startActivity(clientReportsActivityIntent)
        })


    }

    fun getCurrentOffers() {
        CommonFunctionsClass.getCurrentOffers(this)
    }

    fun getUserSettings() {
        CommonFunctionsClass.getUserSettings(this)
    }

    fun getUserTransactions() {
        CommonFunctionsClass.getUserTransactions(this)
    }

    fun getUserRatings() {
        CommonFunctionsClass.getUserRatings(this)
    }

    private fun setErrorMessagesSearchUserProfile(incorrectFields: List<TextUtils.TextInputError>) {
        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        binding.rootLayout.children.forEach loop@ { view ->
            if (view is LinearLayout) {
                view.children.forEach loop@{childView ->
                    if (childView is TextInputLayout) {
                        if (incorrectFields.any { it.layoutId == childView.id }) return@loop
                        childView.isErrorEnabled = false
                    }
                }
            }
        }
    }

    // --------- specific for user with role: renter ---------
    fun getRenterScooters() {
        CommonFunctionsClass.getRenterScooters(this)
    }

    // --------- specific for user with role: renter ---------
    fun getRenterOffers() {
        CommonFunctionsClass.getRenterOffers(this)
    }

    // --------- specific for user with role: renter ---------
    fun getClientReports() {
        userViewModel.getClientReports(user.email)

    }

    // --------- specific for user with roles: client AND renter ---------
    fun getUserChats() {
        CommonFunctionsClass.getUserChats(this)
    }

    fun logout(){
        CommonFunctionsClass.logout(this)
    }

}
