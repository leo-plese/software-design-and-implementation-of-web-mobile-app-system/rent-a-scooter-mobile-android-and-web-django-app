package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class ClientRequest {

    // NOTE: rentOfferId is not needed - ClientRequest class is always a client's request to pre-specified offer

    // chat to which this request belongs
    var chatId: Int = 0

    // rent offer to which this request belongs
    var rentOfferId: Int = 0


    // client who sent request
    var clientEmail: String? = null
    var clientName: String? = null
    var clientSurname: String? = null
    var clientNickname: String = ""

    // text in client request message is OPTIONAL
    var text: String? = null
    var time: Timestamp = Timestamp(System.currentTimeMillis())


}