package com.seven.iznajmiromobil.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class FragmentViewPagerAdapter(fm: FragmentManager, private val items: List<Fragment>) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int = items.size
    override fun getItem(position: Int): Fragment = items[position]
}