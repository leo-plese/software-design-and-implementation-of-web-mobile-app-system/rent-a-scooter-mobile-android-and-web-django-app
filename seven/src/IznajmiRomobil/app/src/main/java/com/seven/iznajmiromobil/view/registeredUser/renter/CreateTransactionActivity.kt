package com.seven.iznajmiromobil.view.registeredUser.renter

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CreateTransactionActivityBinding
import com.seven.iznajmiromobil.models.BasicTransaction
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class CreateTransactionActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: CreateTransactionActivityBinding

    private lateinit var userViewModel: UserViewModel

    private lateinit var transaction: BasicTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_transaction_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subscribeUi()

        /*
        // TODO this part should be in the activity with rent offer details (on "Start transaction" button click)
            - put extra to intent on startActivity for CreateTransactionActivity
        var rentOffer: RenterRentOffer = RenterRentOffer()
        userViewModel.getTransaction(rentOffer.ID)
        -in subscribeUI() in Observer: userViewModel.getTransactionResult -> get result (data: BasicTransaction) and start this activity
        */
        val transactionJson = intent.getStringExtra("transaction")
        transaction = Gson().fromJson(transactionJson, BasicTransaction::class.java)

        setTransactionDetailsUI()
    }

    private fun subscribeUi() {
        userViewModel.confirmTransactionResult.observe(this, Observer{ message ->
            //Toast.makeText(this, "Transaction successfully made at " + message.time + "!", Toast.LENGTH_LONG).show()
            Toast.makeText(this, message.text, Toast.LENGTH_LONG).show()

            finish()
        })
    }

    private fun setTransactionDetailsUI() {
        binding.amountTv.text = transaction.price.toString()
        binding.distanceCoveredTv.text = transaction.distanceCovered.toString()
        binding.penaltyCheckbox.isChecked = transaction.pentalty
        binding.timeTv.text = transaction.time.toString()
    }


    fun confirmTransaction() {
        showPopupConfirmTransaction()
    }

    private fun showPopupConfirmTransaction() {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_make_transaction)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.confirmTransaction(transaction)
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }


    fun discardTransaction() {
        showPopupDiscardTransaction("discardBtn")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> showPopupDiscardTransaction("upBtn")
        }

        return true
    }


    override fun onBackPressed() {
        showPopupDiscardTransaction("backBtn")
        return
    }


    private fun showPopupDiscardTransaction(discardType: String) {

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_transaction)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    when (discardType) {
                        "discardBtn" -> finish()
                        "upBtn" -> NavUtils.navigateUpFromSameTask(this)
                        "backBtn" -> super.onBackPressed()
                    }
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

}
