package com.seven.iznajmiromobil.viewmodel

import androidx.lifecycle.MutableLiveData
import com.seven.iznajmiromobil.models.Transaction
import com.seven.iznajmiromobil.network.api.TransactionApi
import com.seven.iznajmiromobil.network.repository.TransactionRepository
import com.seven.iznajmiromobil.view.base.BaseViewModel
import javax.inject.Inject

class TransactionViewModel @Inject constructor(private val transactionRepository: TransactionRepository) : BaseViewModel() {

    val getUserTransactionsResult = MutableLiveData<Transaction>()

    fun getUserTransactions(userEmail: String) {
        executeTask(transactionRepository.getUserTransactions(userEmail), { data ->
            val response = data as TransactionApi.TransactionResponseBody
            getUserTransactionsResult.value = response.transaction
        })
    }

}