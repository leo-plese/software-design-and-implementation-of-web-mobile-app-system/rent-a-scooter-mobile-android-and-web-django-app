package com.seven.iznajmiromobil.view.scooters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ScootersActivityBinding
import com.seven.iznajmiromobil.models.Scooter
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ScootersActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: ScootersActivityBinding

    private lateinit var adapter: ScootersAdapter
    private val scooterList = mutableListOf<Scooter>()

    private val SHARED_PREF_NAME = "userSharedPrefs"

    private lateinit var sharedPreferences : SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"

    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.scooters_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]


        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)


        val sharedPreferences : SharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        user  = Gson().fromJson(json, User::class.java)
        subscribeUi()
    }

    private fun subscribeUi() {
        viewModel.getRenterScootersResult.observe(this, Observer { scooters ->
            scooterList.clear()
            scooterList.addAll(scooters)
            setupRecycler()
        })
    }

    private fun setupRecycler() {
        adapter = ScootersAdapter(scooterList) {
            scooterClicked(it)
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun scooterClicked(scooter: Scooter) {
        startActivity(ScooterActivity.buildIntent(this, scooter))
    }

    fun registerScooter() {
        startActivity(Intent(this, CreateScooterActivity::class.java))
    }

    override fun onResume() {
        super.onResume()
        viewModel.getRenterScooters(user.email)
    }

}