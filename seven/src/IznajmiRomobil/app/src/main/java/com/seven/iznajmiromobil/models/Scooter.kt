package com.seven.iznajmiromobil.models

import java.io.Serializable

class Scooter : Serializable {

    var id: Int = 0
    var comment: String = ""
    var photos: List<ScooterPhoto> = arrayListOf()
    var isAvailable: Boolean = false
}