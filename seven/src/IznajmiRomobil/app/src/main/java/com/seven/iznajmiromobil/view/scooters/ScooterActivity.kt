package com.seven.iznajmiromobil.view.scooters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ScooterActivityBinding
import com.seven.iznajmiromobil.models.Scooter
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject


class ScooterActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: ScooterActivityBinding

    private lateinit var scooter: Scooter

    companion object {
        fun buildIntent(context: Context, scooter: Scooter): Intent {
            val intent = Intent(context, ScooterActivity::class.java)
            intent.putExtra("scooter", scooter)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.scooter_activity)
        binding.activity = this
        scooter = intent.getSerializableExtra("scooter") as Scooter
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        showData()
        subscribeUi()
    }

    private fun showData() {
        binding.scooterComment.text = scooter.comment
        scooter.photos.forEach { photo ->
            val imageView = ImageView(this)
            imageView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            binding.imagesLayout.addView(imageView)
            Glide.with(imageView).load(photo.imgURL).into(imageView)
        }
    }

    private fun subscribeUi() {
        viewModel.deleteScooterResult.observe(this, Observer {
            finish()
        })
    }

    fun deleteScooter() {
        viewModel.deleteScooter(scooter.id)
    }

    fun editPhotos() {
        startActivity(ScooterPhotosActivity.buildIntent(this, scooter))
    }

}