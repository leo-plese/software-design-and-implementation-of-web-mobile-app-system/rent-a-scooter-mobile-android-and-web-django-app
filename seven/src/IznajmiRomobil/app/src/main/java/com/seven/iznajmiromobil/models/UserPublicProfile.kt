package com.seven.iznajmiromobil.models

class UserPublicProfile {

    var ID: Int = 0

    // email, name and surname CAN be null - if user set their visibility to private (otherwise: if they have public visibility, they are non-null values)
    var email: String? = null
    var name: String? = null
    var surname: String? = null
    // nickname has always public visibility (user cannot change his nickname's visibility) -> cannot be null
    var nickname: String = ""

    // map rating with public data of user who participated in rating (his public profile data)
    var ratingsAsClient: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()
    var ratingsAsRenter: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()

    var ratingAndUserInRatingPublicProfile = RatingAndUserInRatingPublicProfile()

}

