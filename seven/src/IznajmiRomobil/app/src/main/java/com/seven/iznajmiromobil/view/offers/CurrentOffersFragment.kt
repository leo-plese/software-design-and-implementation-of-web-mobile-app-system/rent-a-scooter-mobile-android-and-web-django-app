package com.seven.iznajmiromobil.view.offers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.CurrentOffersFragmentBinding
import com.seven.iznajmiromobil.models.AdvertisedCurrentRentOffer
import com.seven.iznajmiromobil.view.base.BaseFragment
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.sql.Timestamp
import java.util.*
import javax.inject.Inject

class CurrentOffersFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: CurrentOffersFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.current_offers_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        subscribeUi()
        val timestamp = Timestamp(System.currentTimeMillis())
        viewModel.getCurrentOffers(timestamp.toString())
        return binding.root
    }

    private fun subscribeUi() {
        viewModel.getCurrentOffersResult.observe(this, Observer { offers ->
            setupRecyler(offers)
        })
    }

    private fun setupRecyler(offers: List<AdvertisedCurrentRentOffer>) {
        val adapter = CurrentOfferAdapter(offers.toMutableList()) { offer ->
            offerClicked(offer)
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun offerClicked(offer: AdvertisedCurrentRentOffer) {
        startActivity(OfferDetailsActivity.buildIntent(requireContext(), offer.id))
    }
}