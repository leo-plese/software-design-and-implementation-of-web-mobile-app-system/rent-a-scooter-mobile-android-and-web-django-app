package com.seven.iznajmiromobil.network.interceptors

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.seven.iznajmiromobil.IznajmiRomobilApplication
import okhttp3.Interceptor
import okhttp3.Response

class ErrorInterceptor(private val application: IznajmiRomobilApplication) : Interceptor {

    private var canHandleResponse = true
    private val errorHandlingLockTimeout: Long = 3000
    private val genericErrorMessage = "Something went wrong."

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        if (!response.isSuccessful) {
            if (canHandleResponse) {
                response.body()?.source()?.readString(Charsets.UTF_8)?.let { body ->
                    handleError(response.code(), body)
                }
                lockErrorHandling()
            }
        }
        return response
    }

    private fun lockErrorHandling() {
        canHandleResponse = false
        Handler(Looper.getMainLooper()).postDelayed({
            canHandleResponse = true
        }, errorHandlingLockTimeout)
    }

    private fun handleError(responseCode: Int, errorBody: String) {
        Handler(Looper.getMainLooper()).post {
            when (responseCode) {
                else -> handleDefaultError(errorBody)
            }
        }
    }

    private fun handleDefaultError(body: String) {
        Toast.makeText(application, genericErrorMessage, Toast.LENGTH_SHORT).show()
    }

}