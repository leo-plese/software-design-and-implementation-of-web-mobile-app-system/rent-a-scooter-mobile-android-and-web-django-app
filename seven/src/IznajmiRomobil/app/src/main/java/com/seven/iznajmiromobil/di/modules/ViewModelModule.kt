package com.seven.iznajmiromobil.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seven.iznajmiromobil.di.keys.ViewModelKey
import com.seven.iznajmiromobil.utils.ViewModelFactory
import com.seven.iznajmiromobil.viewmodel.TransactionViewModel
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    internal abstract fun bindUserViewModel(viewModel: UserViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TransactionViewModel::class)
    internal abstract fun bindTransactionViewModel(viewModel: TransactionViewModel): ViewModel
}