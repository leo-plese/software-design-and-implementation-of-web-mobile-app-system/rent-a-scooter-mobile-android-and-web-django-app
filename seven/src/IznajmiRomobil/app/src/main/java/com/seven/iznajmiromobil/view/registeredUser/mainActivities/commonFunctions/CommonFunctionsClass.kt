package com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.view.login.LoginActivity
import com.seven.iznajmiromobil.view.myDrives.MyDrivesActivity
import com.seven.iznajmiromobil.view.offers.OffersActivity
import com.seven.iznajmiromobil.view.publicUser.CurrentOffersActivity
import com.seven.iznajmiromobil.view.registeredUser.ChatsActivity
import com.seven.iznajmiromobil.view.registeredUser.RatingsActivity
import com.seven.iznajmiromobil.view.registeredUser.SettingsActivity
import com.seven.iznajmiromobil.view.registeredUser.TransactionsActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.ClientAndRenterMainActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.ClientMainActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.RenterMainActivity
import com.seven.iznajmiromobil.view.registeredUser.renter.ClientReportsActivity
import com.seven.iznajmiromobil.view.scooters.ScootersActivity

// class contains some of the functions which are used in same way by more than one activity
// so that they wouldn't have to be reimplemented each time in the same way
class CommonFunctionsClass {

    // option - user can go directly to current offers list without logging in (like any non-registered user)
    // function for both registered and non-registered users
    // for returning list of active scooter rental offers
    companion object {
        fun startActivityBasedOnRoles(activity: Activity, IS_CLIENT: Boolean, IS_RENTER: Boolean) {
            if (IS_CLIENT == true && IS_RENTER == true)
                activity.startActivity(Intent(activity, ClientAndRenterMainActivity::class.java))
            else if (IS_CLIENT == true)
                activity.startActivity(Intent(activity, ClientMainActivity::class.java))
            else
                activity.startActivity(Intent(activity, RenterMainActivity::class.java))
        }

        fun getCurrentOffers(activity: Activity) {
            activity.startActivity(Intent(activity, CurrentOffersActivity::class.java))
        }

        fun getUserSettings(activity: Activity) {
            activity.startActivity(Intent(activity, SettingsActivity::class.java))
        }

        fun getUserTransactions(activity: Activity) {
            activity.startActivity(Intent(activity, TransactionsActivity::class.java))
        }

        fun getUserRatings(activity: Activity) {
            activity.startActivity(Intent(activity, RatingsActivity::class.java))
        }

        fun getClientReports(activity: Activity) {
            activity.startActivity(Intent(activity, ClientReportsActivity::class.java))
        }

        fun getUserChats(activity: Activity) {
            activity.startActivity(Intent(activity, ChatsActivity::class.java))
        }

        fun getRenterScooters(activity: Activity) {
            activity.startActivity(Intent(activity, ScootersActivity::class.java))
        }

        fun getRenterOffers(activity: Activity) {
            activity.startActivity(Intent(activity, OffersActivity::class.java))
        }

        fun  showNoSearchedUserFoundPopup(activity: Activity, searchUserNickname: String?) {

            AlertDialog.Builder(activity)
                .setCancelable(true)
                .setTitle(R.string.no_searched_user_found)
                .setMessage(activity.resources.getString(R.string.no_searched_user_found_msg, searchUserNickname))
                .setNeutralButton(R.string.ok, null)
                .show()
        }

        fun logout(activity: Activity){
            activity.startActivity(Intent(activity, LoginActivity::class.java))
        }

        //if convenient, put any other functions used in more than one activity
    }
}