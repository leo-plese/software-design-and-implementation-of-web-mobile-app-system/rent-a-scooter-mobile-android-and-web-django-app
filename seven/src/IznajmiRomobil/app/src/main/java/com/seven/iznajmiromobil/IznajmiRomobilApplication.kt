package com.seven.iznajmiromobil

import android.content.Context
import androidx.multidex.MultiDex
import com.seven.iznajmiromobil.di.components.DaggerApplicationComponent
import com.seven.iznajmiromobil.view.base.BaseActivity
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class IznajmiRomobilApplication : DaggerApplication() {

    private val applicationComponent by lazy {
        DaggerApplicationComponent.builder().application(this).build()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationComponent

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    var currentActivity: BaseActivity? = null
}