package com.seven.iznajmiromobil.view.resetPassword

import android.os.Bundle
import android.widget.Toast
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ResetPasswordActivityBinding
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class ResetPasswordActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ResetPasswordActivityBinding

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.reset_password_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        subscribeUi()
    }

    private fun subscribeUi() {
        userViewModel.setPasswordResult.observe(this, Observer { data ->
            Toast.makeText(this, "Password set successfully.", Toast.LENGTH_SHORT).show()
        })
    }

    private fun setErrorMessages(incorrectFields: List<com.seven.iznajmiromobil.utils.TextUtils.TextInputError>) {
        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        binding.rootLayout.children.forEach loop@ { view ->
            if (view is TextInputLayout) {
                if (incorrectFields.any { it.layoutId == view.id }) return@loop
                view.isErrorEnabled = false
            }
        }
    }

    fun setPassword() {
        val incorrectFields = com.seven.iznajmiromobil.utils.TextUtils.isSetPasswordDataValid(
            binding.emailInputEditText.text.toString(),
            binding.passwordInputEditText.text.toString(),
            binding.repeatPasswordInputEditText.text.toString()
        )
        setErrorMessages(incorrectFields)
        if (incorrectFields.isEmpty())
            userViewModel.setPassword(
                binding.emailInputEditText.text.toString(),
                binding.passwordInputEditText.text.toString()
            )
    }

}