package com.seven.iznajmiromobil.view.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.LoginActivityBinding
import com.seven.iznajmiromobil.utils.TextUtils
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.view.registration.RegistrationActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: LoginActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"
    private val REMEMBER_ME_BOOLEAN = "rememberMeBoolean"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)



        // if user data hasn't been remembered - observe login action: when user logs in, do actions (show Toast message)
        subscribeUi()

        //TODO handle UNsuccessful login !!! - ErrorInterceptor - enable specific (not generic) error handling
    }

    override fun onBackPressed() {
        finishAffinity()
    }

    private fun subscribeUi() {
        userViewModel.loginResult.observe(this, Observer { loggedInUser ->
            Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show()

            val editor = sharedPreferences.edit()
            // if "remember me" checkbox is checked - save user data in shared preference
            // else - clear user data previously stored in shared preferences and don't save any user data

            editor.putBoolean(REMEMBER_ME_BOOLEAN, binding.rememberMeCheckbox.isChecked)
                .putString(USER_SHARED_PREF_KEY, Gson().toJson(loggedInUser)).apply()
            //startActivity(Intent(this, ScootersActivity::class.java))
           CommonFunctionsClass.startActivityBasedOnRoles(this, loggedInUser.isClient, loggedInUser.isRenter)
        })
    }


    private fun setErrorMessages(incorrectFields: List<TextUtils.TextInputError>) {
        incorrectFields.forEach { error ->
            val textInputLayout = findViewById<TextInputLayout>(error.layoutId)
            textInputLayout.error = error.errorMessage
        }
        binding.rootLayout.children.forEach loop@{ view ->
            if (view is TextInputLayout) {
                if (incorrectFields.any { it.layoutId == view.id }) return@loop
                view.isErrorEnabled = false
            }
        }
    }

    fun login() {
        val incorrectFields = TextUtils.isLoginDataValid(
            binding.emailInputEditText.text.toString(),
            binding.passwordInputEditText.text.toString()
        )

        setErrorMessages(incorrectFields)
        if (incorrectFields.isEmpty()) {
            userViewModel.login(
                binding.emailInputEditText.text.toString(),
                binding.passwordInputEditText.text.toString()
            )
        }
    }

    fun register() {
        startActivity(Intent(this, RegistrationActivity::class.java))
    }

    // option - user can go directly to current offers list without logging in (like any non-registered user)
    // function for both registered and non-registered users
    // for returning list of active scooter rental offers
    fun getCurrentOffers() {
        CommonFunctionsClass.getCurrentOffers(this)
    }


}