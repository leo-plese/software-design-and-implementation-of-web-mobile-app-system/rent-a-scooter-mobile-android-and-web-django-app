package com.seven.iznajmiromobil.view.offers

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.PastOffersFragmentBinding
import com.seven.iznajmiromobil.models.RenterRentOffer
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseFragment
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class PastOffersFragment: BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: PastOffersFragmentBinding

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private val USER_SHARED_PREF_KEY: String = "userData"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.past_offers_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        val sharedPreferences : SharedPreferences = requireContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user  = Gson().fromJson(json, User::class.java)
        subscribeUi()
        viewModel.getRenterOffers(user.email)
        return binding.root
    }

    private fun subscribeUi() {
        viewModel.getRenterOffersResult.observe(this, Observer { offers ->
            setupRecyler(offers)
        })
    }

    private fun setupRecyler(offers: List<RenterRentOffer>) {
        val adapter = RenterOfferAdapter(offers.toMutableList()) { offer ->
            offerClicked(offer)
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun offerClicked(offer: RenterRentOffer) {
        startActivity(OfferDetailsActivity.buildIntent(requireContext(), offer.id))
    }
}