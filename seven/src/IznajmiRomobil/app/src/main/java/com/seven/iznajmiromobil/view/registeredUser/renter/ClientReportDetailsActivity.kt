package com.seven.iznajmiromobil.view.registeredUser.renter

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.ClientReportDetailsActivityBinding
import com.seven.iznajmiromobil.models.ClientReport
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ImageAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*
import javax.inject.Inject

class ClientReportDetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ClientReportDetailsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private lateinit var clientReport: ClientReport

    private lateinit var removeImageAdapter: ImageAdapter
    private lateinit var removeScooterPhotosGridView: GridView
    private lateinit var removeLoadImagesProgressBar: ProgressBar
    private lateinit var removeLoadScooterPhotosLayout: LinearLayout
    private var removeCouldNotLoad = false
    private var removeLoaded = false

    private lateinit var addImageAdapter: ImageAdapter
    private lateinit var addScooterPhotosGridView: GridView
    private lateinit var addLoadImagesProgressBar: ProgressBar
    private lateinit var addLoadScooterPhotosLayout: LinearLayout
    private var addCouldNotLoad = false
    private var addLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.client_report_details_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subscribeUi()


        val clientReportJson = intent.getStringExtra("clientReport")
        clientReport = Gson().fromJson(clientReportJson, ClientReport::class.java)


        setReportDetailsUI(clientReport)
    }

    private fun subscribeUi() {
        userViewModel.answerClientReportForOfferResult.observe(this, Observer{ acceptDenyMessage ->
            Toast.makeText(this, "Report answer recorded!", Toast.LENGTH_SHORT).show()
            NavUtils.navigateUpFromSameTask(this)
            // if acceptDenyMessage is not null - renter accepted report, else denied (and forwarded to admin)
        })
    }

    fun acceptReport() {
        showAcceptOrDenyReportPopup(true)
    }

    fun denyReport() {
        showAcceptOrDenyReportPopup(false)
    }

    private fun showAcceptOrDenyReportPopup(answerYesNo: Boolean) {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setTitle(R.string.discard_changes)
            .setMessage(R.string.are_you_sure_discard_account_roles_changes)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.answerClientReportForOffer(clientReport.rentOfferId, answerYesNo)
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

    private fun setReportDetailsUI(clientReport: ClientReport) {
        binding.userNicknameTv.text = clientReport.clientNickname
        binding.reportTextTv.text = clientReport.cause
        binding.reportTimeTv.text = clientReport.time.toString()



        removeLoadScooterPhotosLayout = binding.loadScooterPhotosToRemoveLayout
        removeLoadImagesProgressBar = binding.removeImagesLoadProgressBar

        removeScooterPhotosGridView = binding.scooterPhotosToRemoveGrid

        val removeImgBitmapList: ArrayList<Bitmap?> = arrayListOf()
        removeImageAdapter = ImageAdapter(removeImgBitmapList, this)
        removeScooterPhotosGridView.adapter = removeImageAdapter




        val removeScooterPhotosObjectsList: List<ScooterPhoto> = clientReport.photosToRemove

        val removeScooterPhotosList: ArrayList<String> = arrayListOf()

        removeScooterPhotosObjectsList.forEach { scooterPhoto ->
            removeScooterPhotosList.add(scooterPhoto.imgURL)
        }

        val removeScooterPhotosArray = arrayOfNulls<String>(removeScooterPhotosList.size)
        for (i in 0 until removeScooterPhotosList.size) {
            removeScooterPhotosArray[i] = removeScooterPhotosList.get(i)
        }

        val removeRetrieveImageFromUrlAsyncTask = RetrieveImageFromUrlAsyncTask("remove", removeLoadImagesProgressBar, removeImageAdapter, removeLoaded)
        removeRetrieveImageFromUrlAsyncTask.execute(*removeScooterPhotosArray)

        Handler().postDelayed(Runnable {
            if (!removeLoaded) {
                removeRetrieveImageFromUrlAsyncTask.cancel(true)
                removeCouldNotLoadImages()
            }
        }, 15000L)




        addLoadScooterPhotosLayout = binding.loadScooterPhotosToAddLayout
        addLoadImagesProgressBar = binding.addImagesLoadProgressBar

        addScooterPhotosGridView = binding.scooterPhotosToAddGrid

        val addImgBitmapList: ArrayList<Bitmap?> = arrayListOf()
        addImageAdapter = ImageAdapter(addImgBitmapList, this)
        addScooterPhotosGridView.adapter = addImageAdapter

        val addScooterPhotosObjectsList: List<ScooterPhoto> = clientReport.photosToAdd

        val addScooterPhotosList: ArrayList<String> = arrayListOf()

        addScooterPhotosObjectsList.forEach { scooterPhoto ->
            addScooterPhotosList.add(scooterPhoto.imgURL)
        }

        val addScooterPhotosArray = arrayOfNulls<String>(addScooterPhotosList.size)
        for (i in 0 until addScooterPhotosList.size) {
            addScooterPhotosArray[i] = addScooterPhotosList.get(i)
        }

        val addRetrieveImageFromUrlAsyncTask = RetrieveImageFromUrlAsyncTask("add", addLoadImagesProgressBar, addImageAdapter, addLoaded)
        addRetrieveImageFromUrlAsyncTask.execute(*addScooterPhotosArray)

        Handler().postDelayed(Runnable {
            if (!addLoaded) {
                addRetrieveImageFromUrlAsyncTask.cancel(true)
                addCouldNotLoadImages()
            }
        }, 15000L)



    }


    inner class RetrieveImageFromUrlAsyncTask(var photoType: String, var loadImagesProgressBar: ProgressBar, var imageAdapter: ImageAdapter, var loaded: Boolean) : AsyncTask<String, Int, ArrayList<Bitmap>>() {

        override fun doInBackground(vararg imgUrls: String): ArrayList<Bitmap> {

            var bitmapList: ArrayList<Bitmap> = arrayListOf()
            val count = imgUrls.size

            for (i in 0 until count) {
                var bitmap: Bitmap? = null
                try {
                    val inStream: InputStream = URL(imgUrls[i]).openStream()
                    bitmap = BitmapFactory.decodeStream(inStream)
                    if (bitmap != null)
                        bitmapList.add(bitmap)
                    publishProgress(((i + 1) / count.toFloat() * 100).toInt())
                } catch (ex: IOException) { return arrayListOf() }

            }
            return bitmapList
        }

        override fun onProgressUpdate(vararg values: Int?) {
            loadImagesProgressBar.progress = values[0]!!
            //Toast.makeText(this@ClientReportDetailsActivity,"Downloaded ${values[0]} %",Toast.LENGTH_SHORT).show()
            super.onProgressUpdate(values[0])
        }
        override fun onPostExecute(result: ArrayList<Bitmap>) {
            super.onPostExecute(result)
            if (result.isEmpty()) {
                if (photoType.equals("remove"))
                    removeCouldNotLoadImages()
                else
                    addCouldNotLoadImages()
            } else {
                imageAdapter.addAll(result)
                if (photoType.equals("remove"))
                    removeLoaded = true
                else
                    addLoaded = true
                //loaded = true
            }
        }

    }


    private fun removeCouldNotLoadImages() {
        if (removeCouldNotLoad)
            return

        var removeCouldNotLoadPhotosTv = findViewById<TextView>(R.id.remove_could_not_load_scooter_photos_tv)
        removeCouldNotLoadPhotosTv.visibility = View.VISIBLE
        removeLoadScooterPhotosLayout.visibility = View.GONE
        removeCouldNotLoadPhotosTv.text = "Cannot load scooter images"
        Toast.makeText(this@ClientReportDetailsActivity, "Cannot load scooter images", Toast.LENGTH_SHORT).show()

        removeCouldNotLoad = true
    }

    private fun addCouldNotLoadImages() {
        if (addCouldNotLoad)
            return

        var addCouldNotLoadPhotosTv = findViewById<TextView>(R.id.add_could_not_load_scooter_photos_tv)
        addCouldNotLoadPhotosTv.visibility = View.VISIBLE
        addLoadScooterPhotosLayout.visibility = View.GONE
        addCouldNotLoadPhotosTv.text = "Cannot load scooter images"
        Toast.makeText(this@ClientReportDetailsActivity, "Cannot load scooter images", Toast.LENGTH_SHORT).show()

        addCouldNotLoad = true
    }
}
