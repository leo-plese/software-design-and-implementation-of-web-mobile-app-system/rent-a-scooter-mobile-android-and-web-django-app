package com.seven.iznajmiromobil.network.repository

import com.seven.iznajmiromobil.network.api.ClientApi
import com.seven.iznajmiromobil.network.api.RenterApi
import com.seven.iznajmiromobil.network.api.UserApi

class UserRepository(private val userApi: UserApi, private val renterApi: RenterApi, private val clientApi: ClientApi) {

    fun login(loginRequstBody: UserApi.LoginRequestBody) = userApi.login(loginRequstBody)
    fun register(registerRequestBody: UserApi.RegisterRequestBody) = userApi.register(registerRequestBody)
    fun resetPassword(resetPasswordRequestBody: UserApi.ResetPasswordRequestBody) = userApi.resetPassword(resetPasswordRequestBody)
    fun setPassword(resetPasswordRequestBody: UserApi.LoginRequestBody) = userApi.setPassword(resetPasswordRequestBody)

    fun deleteUser(deleteUserBody : UserApi.EmailUserBody) = userApi.deleteUser(deleteUserBody)
    fun getAccountData( getAccountDataBody: UserApi.EmailUserBody) = userApi.getAccountData(getAccountDataBody)
    //fun changeAccountData( changeAccountDataBody: UserApi.UserAccountDataBody) = userApi.changeAccountData(changeAccountDataBody)
    //fun getAccountDataVisibility( getAccountDataVisibilityBody: UserApi.EmailUserBody) = userApi.getAccountDataVisibility(getAccountDataVisibilityBody)
    //fun changeAccountDataVisibility( changeAccountDataVisibilityBody: UserApi.UserAccountDataVisibilityBody) = userApi.changeAccountDataVisibility(changeAccountDataVisibilityBody)
    fun changeAccountData(changeAccountDataAndDataVisibilityBody: UserApi.UserAccountDataAndDataVisibilityBody) = userApi.changeAccountData(changeAccountDataAndDataVisibilityBody)
    fun getUserTransactions( getUserTransactionsBody: UserApi.EmailUserBody) = userApi.getUserTransactions(getUserTransactionsBody)
    fun changeUserRoles( userRolesBody: UserApi.UserRolesBody) = userApi.changeUserRoles(userRolesBody)
    fun getUserPublicProfile( nicknameUserBody: UserApi.NicknameUserBody) = userApi.getUserPublicProfile(nicknameUserBody)
    fun getUserRatings( emailUserBody: UserApi.EmailUserBody) = userApi.getUserRatings(emailUserBody)
    fun getCurrentOffers(timeStampBody: RenterApi.StringTimeStampBody) = userApi.getCurrentOffers(timeStampBody)
    fun sendMessageInChat(sendMessageInChatRequestBody: UserApi.SendMessageInChatRequestBody) = userApi.sendMessageInChat(sendMessageInChatRequestBody)
    fun getMessagesInChat(getMessagesInChatRequestBody: UserApi.GetMessagesInChatRequestBody) = userApi.getMessagesInChat(getMessagesInChatRequestBody)
    fun getChats(emailUserBody: UserApi.EmailUserBody) = userApi.getChats(emailUserBody)
    fun getOfferDetails(offerId: UserApi.OfferDetailsBody) = userApi.getOfferDetails(offerId)


    fun getRenterOffers(emailUserBody: UserApi.EmailUserBody) = renterApi.getRenterOffers(emailUserBody)
    fun createOffer(createOfferBody: RenterApi.CreateOfferBody) = renterApi.createOffer(createOfferBody)
    fun createScooter(createScooterBody: RenterApi.CreateScooterBody) = renterApi.createScooter(createScooterBody)
    fun deleteScooter(idScooterBody: RenterApi.IDScooterBody) = renterApi.deleteScooter(idScooterBody)
    fun getRenterScooters(emailUserBody: UserApi.EmailUserBody) = renterApi.getRenterScooters(emailUserBody)
    fun addScooterPhotos(addScooterPhotosBody: RenterApi.AddOrDeleteScooterPhotosBody) = renterApi.addScooterPhoto(addScooterPhotosBody)
    fun deleteScooterPhotos(deleteScooterPhotosBody: RenterApi.AddOrDeleteScooterPhotosBody) = renterApi.deleteScooterPhotos(deleteScooterPhotosBody)
    fun getTransaction(getTransactionRequestBody: RenterApi.GetTransactionRequestBody) = renterApi.getTransaction(getTransactionRequestBody)
    fun confirmTransaction(confirmTransactionRequestBody: RenterApi.ConfirmTransactionRequestBody) = renterApi.confirmTransaction(confirmTransactionRequestBody)
    fun createRating(createRatingRequestBody: RenterApi.CreateRatingRequestBody) = renterApi.createRating(createRatingRequestBody)
    fun getClientRequests(getClientRequestsRequestBody: RenterApi.GetClientRequestsRequestBody) = renterApi.getClientRequests(getClientRequestsRequestBody)
    fun confirmClientRequestForOffer(confirmClientRequestForOfferRequestBody: RenterApi.ConfirmClientRequestForOfferRequestBody) = renterApi.confirmClientRequestForOffer(confirmClientRequestForOfferRequestBody)
    fun getClientReports(emailUserBody: UserApi.EmailUserBody) = renterApi.getClientReports(emailUserBody)
    fun answerClientReportForOffer(answerClientReportForOfferRequestBody: RenterApi.AnswerClientReportForOfferRequestBody) = renterApi.answerClientReportForOffer(answerClientReportForOfferRequestBody)


    fun getClientOffers(emailUserBody: UserApi.EmailUserBody) = clientApi.getClientOffers(emailUserBody)
    fun sendRequestForOffer(sendRequestForOfferRequestBody: ClientApi.SendRequestForOfferRequestBody) = clientApi.sendRequestForOffer(sendRequestForOfferRequestBody)
    fun sendReportForOffer(sendReportForOfferRequestBody: ClientApi.SendReportForOfferRequestBody) = clientApi.sendReportForOffer(sendReportForOfferRequestBody)


}