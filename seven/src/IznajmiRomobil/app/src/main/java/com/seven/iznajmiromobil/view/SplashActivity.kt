package com.seven.iznajmiromobil.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.view.doOnLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.SplashActivityBinding
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.login.LoginActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: SplashActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"
    private val REMEMBER_ME_BOOLEAN = "rememberMeBoolean"

    private val handler: Handler = Handler()
    private var isCanceled: Boolean = false


    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"

    private var isRemembered: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.splash_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        //binding.splashLogo.scaleX = -1f



        binding.root.doOnLayout {
            binding.splashLogo.animate()
                .x(binding.root.width.toFloat()/2 + binding.splashLogo.x)
                .setDuration(1000)
                .setInterpolator(AccelerateDecelerateInterpolator())
                .setListener(object : AnimatorListenerAdapter(){
                    override fun onAnimationEnd(animation: Animator?) {
                        if(isCanceled){
                            finish()
                        } else{
                            handler.postDelayed({
                                subscribeUi()
                                // get stored user data
                                val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
                                val rememberedUser = Gson().fromJson(json, User::class.java)

                                // get authorization by logging in
                                if (sharedPreferences.getBoolean(REMEMBER_ME_BOOLEAN, false)) {
                                    userViewModel.login(rememberedUser.email, rememberedUser.password)
                                }
                                else {
                                    startActivity(Intent(this@SplashActivity, LoginActivity::class.java))

                                    finish()

                                }
                            }, 1000)
                        }
                    }
                })
                .start()
        }


        //TODO handle UNsuccessful login !!! - ErrorInterceptor - enable specific (not generic) error handling
    }

    private fun subscribeUi() {
        userViewModel.loginResult.observe(this, Observer { data ->
            val user = data
            handleUserLogin(user)
        })
    }

    private fun handleUserLogin(user: User) {
        var IS_CLIENT = false
        var IS_RENTER = false


        // update shared prefs with possibly new user roles
        val editor = sharedPreferences.edit()

        // check if user is active
        // if he is NOT active - means admin banned him from using the app - clear cache and show banned_user_layout popup and on button click close the application.
        if (!user.isActive) {
            editor.clear()
            editor.clear()

            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(R.string.banned_user)
                .setMessage(R.string.banned_user_msg)
                .setNeutralButton(
                    R.string.close_app,
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        finish()
                    })
                .show()

        } else {
            editor.putString(USER_SHARED_PREF_KEY, Gson().toJson(user))
            IS_CLIENT = user.isClient
            IS_RENTER = user.isRenter
            editor.putBoolean(IS_CLIENT_KEY, IS_CLIENT)
            editor.putBoolean(IS_RENTER_KEY, IS_RENTER)
        }

        editor.apply()

        // if user data has been stored (user has checked "Remember me" option the last time he logged in) - get stored data and get information about user roles
        // and set client_main_activity layout
        // else - information about user roles is unknown (IS_CLIENT and IS_RENTER initialized to false) so
        // just set login_activity layout

        handler.postDelayed(Runnable {
            if (sharedPreferences.getBoolean(REMEMBER_ME_BOOLEAN, false)) {
                //       startActivity(Intent(this, ScootersActivity::class.java))
                CommonFunctionsClass.startActivityBasedOnRoles(this, IS_CLIENT, IS_RENTER)
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
            finish()

        }, 500L)
    }

    override fun onPause() {
        isCanceled = true
        super.onPause()
    }

    override fun onResume() {
        handler.removeCallbacksAndMessages(null)
        super.onResume()
    }

}
