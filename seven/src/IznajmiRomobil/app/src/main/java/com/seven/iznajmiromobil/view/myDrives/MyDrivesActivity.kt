package com.seven.iznajmiromobil.view.myDrives

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.MyDrivesActivityBinding
import com.seven.iznajmiromobil.models.RenterRentOffer
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.offers.OfferDetailsActivity
import com.seven.iznajmiromobil.view.offers.RenterOfferAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class MyDrivesActivity: BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: MyDrivesActivityBinding
    private lateinit var viewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.my_drives_activity)
        binding.activity = this
        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]
        subscribeUi()
        viewModel.getClientOffers(intent.getStringExtra("email"))
    }

    private fun subscribeUi() {
        viewModel.getClientOffersResult.observe(this, Observer {
            setupRecycler(it)
        })
    }

    private fun setupRecycler(drives: List<RenterRentOffer>) {
        val adapter = RenterOfferAdapter(drives.toMutableList()) {
            openOffer(it)
        }
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun openOffer(offer: RenterRentOffer) {
        startActivity(OfferDetailsActivity.buildIntent(this, offer.id))
    }

}