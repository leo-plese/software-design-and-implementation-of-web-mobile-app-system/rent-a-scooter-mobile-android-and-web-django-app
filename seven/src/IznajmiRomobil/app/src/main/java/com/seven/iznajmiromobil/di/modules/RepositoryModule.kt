package com.seven.iznajmiromobil.di.modules

import com.seven.iznajmiromobil.di.scopes.ApplicationScope
import com.seven.iznajmiromobil.network.api.ClientApi
import com.seven.iznajmiromobil.network.api.RenterApi
import com.seven.iznajmiromobil.network.api.TransactionApi
import com.seven.iznajmiromobil.network.api.UserApi
import com.seven.iznajmiromobil.network.repository.TransactionRepository
import com.seven.iznajmiromobil.network.repository.UserRepository
import dagger.Module
import dagger.Provides

@Module(includes = [ApiModule::class])
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideUserRepository(userApi: UserApi, renterApi: RenterApi, clientApi: ClientApi) = UserRepository(userApi, renterApi, clientApi)

    @Provides
    @ApplicationScope
    fun provideTransactionRepository(transactionApi: TransactionApi) = TransactionRepository(transactionApi)
    
}