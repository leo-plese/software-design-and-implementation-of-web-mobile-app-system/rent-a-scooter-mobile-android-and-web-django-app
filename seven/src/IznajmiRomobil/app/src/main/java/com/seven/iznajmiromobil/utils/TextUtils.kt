package com.seven.iznajmiromobil.utils

import android.util.Patterns
import com.seven.iznajmiromobil.R

object TextUtils {

    data class TextInputError(val layoutId: Int, val errorMessage: String)

    fun isLoginDataValid(email: String, password: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (password.length < 6) invalidFields.add(TextInputError(R.id.password_input_layout, "Password must be at least 6 characters"))
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) invalidFields.add(TextInputError(R.id.email_input_layout, "Enter a valid email address"))
        return invalidFields
    }

    fun isRegisterDataValid(name: String, surname: String, username: String, email: String, creditCardNumber: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (name.isBlank()) invalidFields.add(TextInputError(R.id.name_input_layout, "Name must not be blank"))
        if (surname.isBlank()) invalidFields.add(TextInputError(R.id.surname_input_layout, "Surname must not be blank"))
        if (username.isBlank()) invalidFields.add(TextInputError(R.id.username_input_layout, "Username must not be blank"))
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) invalidFields.add(TextInputError(R.id.email_input_layout, "Enter a valid email address"))
        if (creditCardNumber.length < 8) invalidFields.add(TextInputError(R.id.card_input_layout, "Enter a valid card number"))
        return invalidFields
    }

    fun isSetPasswordDataValid(email: String, password: String, repeatPassword: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (password.length < 6) invalidFields.add(TextInputError(R.id.password_input_layout, "Password must be at least 6 characters"))
        if (password != repeatPassword) invalidFields.add(TextInputError(R.id.repeat_password_input_layout, "Passwords do not match"))
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) invalidFields.add(TextInputError(R.id.email_input_layout, "Enter a valid email address"))
        return invalidFields
    }

    fun isChangePasswordDataValid(oldPassword: String, newPassword: String, retypeNewPassword: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (oldPassword.length < 6) invalidFields.add(TextInputError(R.id.old_password_input_layout, "Password must be at least 6 characters"))
        if (newPassword.length < 6) invalidFields.add(TextInputError(R.id.new_password_input_layout, "Password must be at least 6 characters"))
        if (retypeNewPassword.length < 6) invalidFields.add(TextInputError(R.id.retype_new_password_input_layout, "Password must be at least 6 characters"))

        return invalidFields
    }

    fun isChangeAccountPersonalAndVisibilityDataValid(name: String, surname: String, nickname: String, creditCardNumber: String, email: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (name.isBlank()) invalidFields.add(TextInputError(R.id.name_input_layout, "Name must not be blank"))
        if (surname.isBlank()) invalidFields.add(TextInputError(R.id.surname_input_layout, "Surname must not be blank"))
        if (nickname.isBlank()) invalidFields.add(TextInputError(R.id.nickname_input_layout, "Username must not be blank"))
        if (creditCardNumber.length < 8) invalidFields.add(TextInputError(R.id.credit_card_number_input_layout, "Enter a valid card number"))
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) invalidFields.add(TextInputError(R.id.email_input_layout, "Enter a valid email address"))

        return invalidFields
    }


    fun isSearchUserNicknameValid(nickname: String): List<TextInputError> {
        val invalidFields = mutableListOf<TextInputError>()
        if (nickname.isBlank()) invalidFields.add(TextInputError(R.id.nickname_input_layout, "Username must not be blank"))

        return invalidFields
    }

}