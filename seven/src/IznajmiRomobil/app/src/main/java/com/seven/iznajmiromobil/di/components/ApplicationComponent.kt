package com.seven.iznajmiromobil.di.components

import com.seven.iznajmiromobil.di.modules.ApplicationModule
import com.seven.iznajmiromobil.di.scopes.ApplicationScope
import com.seven.iznajmiromobil.IznajmiRomobilApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent : AndroidInjector<IznajmiRomobilApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: IznajmiRomobilApplication): Builder

        fun build(): ApplicationComponent
    }

    override fun inject(konzumApplication: IznajmiRomobilApplication)
}