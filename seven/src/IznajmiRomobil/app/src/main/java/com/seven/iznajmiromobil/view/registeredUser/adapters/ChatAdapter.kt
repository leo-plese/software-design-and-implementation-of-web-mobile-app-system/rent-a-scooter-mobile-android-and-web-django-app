package com.seven.iznajmiromobil.view.registeredUser.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.Chat


class ChatAdapter(var items: List<Chat>) :
    RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {

    var onItemClick: ((Chat) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.chat_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.findViewById<Button>(R.id.go_to_offer__btn).setOnClickListener {
                onItemClick?.invoke(items[adapterPosition])
            }
        }
        fun bind(chat: Chat) {
            itemView.findViewById<TextView>(R.id.offer_id_tv).text = chat.rentOfferId.toString()
            itemView.findViewById<TextView>(R.id.nickname_tv).text = chat.userNickname
        }
    }
}