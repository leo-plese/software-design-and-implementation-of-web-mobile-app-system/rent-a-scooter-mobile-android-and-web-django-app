package com.seven.iznajmiromobil.view.registeredUser

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.RatingsActivityBinding
import com.seven.iznajmiromobil.models.RatingAndUserInRatingPublicProfile
import com.seven.iznajmiromobil.models.User
import com.seven.iznajmiromobil.models.UserRatings
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.RatingAndUserInRatingPublicProfileAdapter
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class RatingsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: RatingsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"


    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"


    private lateinit var ratingsAsClientAdapter: RatingAndUserInRatingPublicProfileAdapter

    private lateinit var ratingsAsRenterAdapter: RatingAndUserInRatingPublicProfileAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.ratings_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()



        val onRatingListViewItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val ratingAndUserInRatingPublicProfile = parent.getItemAtPosition(position) as RatingAndUserInRatingPublicProfile

            searchUserProfile(ratingAndUserInRatingPublicProfile.userInRatingNickname)
        }

        // 1st ListView - ratings as client
        val ratingsAsClientList: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()

        val ratingsAsClientListView = binding.ratingsAsClientLayout
        ratingsAsClientAdapter = RatingAndUserInRatingPublicProfileAdapter(ratingsAsClientList, this)
        ratingsAsClientListView.adapter = ratingsAsClientAdapter

        ratingsAsClientListView.onItemClickListener = onRatingListViewItemClickListener


        // 2nd ListView - ratings as renter
        val ratingsAsRenterList: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()

        val ratingsAsRenterListView = binding.ratingsAsRenterLayout
        ratingsAsRenterAdapter = RatingAndUserInRatingPublicProfileAdapter(ratingsAsRenterList, this)
        ratingsAsRenterListView.adapter = ratingsAsRenterAdapter

        ratingsAsRenterListView.onItemClickListener = onRatingListViewItemClickListener



        // get stored user data
        val json = sharedPreferences.getString(USER_SHARED_PREF_KEY, null)
        val user  = Gson().fromJson(json, User::class.java)

        userViewModel.getUserRatings(user.email)
    }

    private fun subscribeUi() {

        // when clicked on a list item, display public profile of the user who rated / was rated by the currently displayed user
        userViewModel.getUserRatingsResult.observe(this, Observer{ userRatings ->
            ratingsAsClientAdapter.addAll(userRatings.ratingsAsClient)
            ratingsAsRenterAdapter.addAll(userRatings.ratingsAsRenter)

            setUserRatingDetailsUI(userRatings)
        })

        // when clicked on a list item, display public profile of the user who rated / was rated by the currently displayed user
        userViewModel.getUserPublicProfileResult.observe(this, Observer{ userPublicProfile ->
            val userPublicProfileActivityIntent = Intent(this, UserPublicProfileActivity::class.java)
            userPublicProfileActivityIntent.putExtra("userProfile", Gson().toJson(userPublicProfile))
            userPublicProfileActivityIntent.putExtra("sourceActivity", "RatingsActivity")
            startActivity(userPublicProfileActivityIntent)

            finish()
        })
    }

    private fun searchUserProfile(clickedUserNickname: String) {
        userViewModel.getUserPublicProfile(clickedUserNickname)
    }


    private fun setUserRatingDetailsUI(userRatings: UserRatings) {
        if (userRatings.ratingsAsClient.isEmpty()) {
            binding.noRatingsAsClient.visibility = View.VISIBLE
            binding.ratingsAsClientLayout.visibility = View.GONE
        } else {
            // this is by default
            binding.noRatingsAsClient.visibility = View.GONE
            binding.ratingsAsClientLayout.visibility = View.VISIBLE
        }

        if (userRatings.ratingsAsRenter.isEmpty()) {
            binding.noRatingsAsRenter.visibility = View.VISIBLE
            binding.ratingsAsRenterLayout.visibility = View.GONE
        } else {
            // this is by default
            binding.noRatingsAsRenter.visibility = View.GONE
            binding.ratingsAsRenterLayout.visibility = View.VISIBLE
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed()
        }

        return true
    }

    override fun onBackPressed() {
        doOnUpAndBackButtonPressed()
    }

    private fun doOnUpAndBackButtonPressed() {
        val IS_CLIENT = sharedPreferences.getBoolean(IS_CLIENT_KEY, false)
        val IS_RENTER = sharedPreferences.getBoolean(IS_RENTER_KEY, false)
        CommonFunctionsClass.startActivityBasedOnRoles(this, IS_CLIENT, IS_RENTER)

        finish()
    }

}
