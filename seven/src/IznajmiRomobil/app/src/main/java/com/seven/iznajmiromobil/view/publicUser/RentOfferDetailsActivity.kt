package com.seven.iznajmiromobil.view.publicUser

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.RentOfferDetailsActivityBinding
import com.seven.iznajmiromobil.models.AdvertisedCurrentRentOffer
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.ChatsActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ImageAdapter
import com.seven.iznajmiromobil.view.registeredUser.client.SendRequestActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*
import javax.inject.Inject

class RentOfferDetailsActivity : BaseActivity()  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: RentOfferDetailsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences

    private val USER_SHARED_PREF_KEY: String = "userData"
    private val IS_CLIENT_KEY: String = "IS_CLIENT"

    private val CLIENT_REQUEST_ALREADY_SENT_KEY: String = "CLIENT_REQUEST_ALREADY_SENT"


    private lateinit var offer: AdvertisedCurrentRentOffer
    private lateinit var sourceActivity: String

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var scooterPhotosGridView: GridView
    private lateinit var loadImagesProgressBar: ProgressBar
    private lateinit var loadScooterPhotosLayout: LinearLayout
    private var couldNotLoad = false
    private var loaded = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.rent_offer_details_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        val offerJson = intent.getStringExtra("offer")
        offer = Gson().fromJson(offerJson, AdvertisedCurrentRentOffer::class.java)

        val IS_CLIENT = sharedPreferences.getBoolean(IS_CLIENT_KEY, false)
        val REQUEST_ALREADY_SENT = sharedPreferences.getBoolean(CLIENT_REQUEST_ALREADY_SENT_KEY, true)
        if (IS_CLIENT && !REQUEST_ALREADY_SENT) {
            binding.sendRequestBtn.visibility = View.VISIBLE

            val editor = sharedPreferences.edit()
            editor.putBoolean(CLIENT_REQUEST_ALREADY_SENT_KEY, true)
            editor.commit()
        }

        setOfferDetailsUI(offer)
    }

    fun sendRequest() {
        val sendRequestActivityIntent = Intent(this, SendRequestActivity::class.java)
        sendRequestActivityIntent.putExtra("rentOfferId", offer.id)
        startActivity(Intent(this, SendRequestActivity::class.java))

    }


    private fun setOfferDetailsUI(offer: AdvertisedCurrentRentOffer) {
        binding.startLocationLatitudeTv.text = offer.currentLocationLatitude.toString()
        binding.startLocationLongitudeTv.text = offer.currentLocationLongitude.toString()
        binding.returnLocationLatitudeTv.text = offer.returnLocationLatitude.toString()
        binding.returnLocationLongitudeTv.text = offer.returnLocationLongitude.toString()
        binding.returnTimeTv.text = offer.returnTime.toString()
        binding.unitPriceTv.text = offer.priceKm.toString()
        binding.returnPenaltyTv.text = offer.returnPenalty.toString()
        binding.scooterCommentTv.text = offer.scooterComment


        loadScooterPhotosLayout = binding.loadScooterPhotosLayout
        loadImagesProgressBar = binding.imageLoadProgressBar

        scooterPhotosGridView = binding.scooterPhotosGrid

        val imgBitmapList: java.util.ArrayList<Bitmap?> = arrayListOf()
        imageAdapter = ImageAdapter(imgBitmapList, this)
        scooterPhotosGridView.adapter = imageAdapter




        val scooterPhotosObjectsList: List<ScooterPhoto> = offer.scooterPhotos

        val scooterPhotosList: ArrayList<String> = arrayListOf()

        scooterPhotosObjectsList.forEach { scooterPhoto ->
            scooterPhotosList.add(scooterPhoto.imgURL)
        }

        val scooterPhotosArray = arrayOfNulls<String>(scooterPhotosList.size)
        for (i in 0 until scooterPhotosList.size) {
            scooterPhotosArray[i] = scooterPhotosList.get(i)
        }

        val retrieveImageFromUrlAsyncTask = RetrieveImageFromUrlAsyncTask()
        retrieveImageFromUrlAsyncTask.execute(*scooterPhotosArray)

        Handler().postDelayed(Runnable {
            if (!loaded) {
                retrieveImageFromUrlAsyncTask.cancel(true)
                couldNotLoadImages()
            }
        }, 15000L)

    }


    inner class RetrieveImageFromUrlAsyncTask : AsyncTask<String, Int, ArrayList<Bitmap>>() {

        override fun doInBackground(vararg imgUrls: String): ArrayList<Bitmap> {

            var bitmapList: ArrayList<Bitmap> = arrayListOf()
            val count = imgUrls.size

            for (i in 0 until count) {
                var bitmap: Bitmap? = null
                try {
                    val inStream: InputStream = URL(imgUrls[i]).openStream()
                    bitmap = BitmapFactory.decodeStream(inStream)
                    if (bitmap != null)
                        bitmapList.add(bitmap)
                    publishProgress(((i + 1) / count.toFloat() * 100).toInt())
                } catch (ex: IOException) { return arrayListOf() }

            }
            return bitmapList
        }

        override fun onProgressUpdate(vararg values: Int?) {
            loadImagesProgressBar.progress = values[0]!!
            //Toast.makeText(this@TransactionDetailsActivity,"Downloaded ${values[0]} %",Toast.LENGTH_SHORT).show()
            super.onProgressUpdate(values[0])
        }
        override fun onPostExecute(result: ArrayList<Bitmap>) {
            super.onPostExecute(result)
            if (result.isEmpty()) {
                couldNotLoadImages()
            } else {
                imageAdapter.addAll(result)
                loaded = true
            }
        }

    }


    private fun couldNotLoadImages() {
        if (couldNotLoad)
            return

        var couldNotLoadPhotosTv = findViewById<TextView>(R.id.could_not_load_scooter_photos_tv)
        couldNotLoadPhotosTv.visibility = View.VISIBLE
        loadScooterPhotosLayout.visibility = View.GONE
        couldNotLoadPhotosTv.text = "Cannot load scooter images"
        Toast.makeText(this@RentOfferDetailsActivity, "Cannot load scooter images", Toast.LENGTH_SHORT)
            .show()

        couldNotLoad = true
    }

}
