package com.seven.iznajmiromobil.models

class User {

    var id: Int = 0

    var isActive: Boolean = false
    var email: String = ""
    var password: String = ""

    var name: String = ""
    var surname: String = ""
    var nickname: String = ""
    var creditCardNumber: String = ""

    var isRenter: Boolean = false
    var isClient: Boolean = false

    var idURL: String = ""
    var recordURL: String = ""

    
    var showName: Boolean = false
    var showSurname: Boolean = false
    val showNickname: Boolean = true
    var showEmail: Boolean = false


}