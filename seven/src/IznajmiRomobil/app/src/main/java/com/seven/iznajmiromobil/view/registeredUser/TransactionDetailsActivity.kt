package com.seven.iznajmiromobil.view.registeredUser

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.TransactionDetailsActivityBinding
import com.seven.iznajmiromobil.models.ScooterPhoto
import com.seven.iznajmiromobil.models.Transaction
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.ImageAdapter
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*
import javax.inject.Inject


class TransactionDetailsActivity  : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: TransactionDetailsActivityBinding

    private lateinit var userViewModel: UserViewModel

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var scooterPhotosGridView: GridView
    private lateinit var loadImagesProgressBar: ProgressBar
    private lateinit var loadScooterPhotosLayout: LinearLayout
    private var couldNotLoad = false
    private var loaded = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.transaction_details_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val transactionJson = intent.getStringExtra("transaction")
        val transaction =  Gson().fromJson(transactionJson, Transaction::class.java)

        setTransactionDetailsUI(transaction)
    }

    private fun setTransactionDetailsUI(transaction: Transaction) {
        binding.amountTv.text = transaction.price.toString()
        binding.distanceCoveredTv.text = transaction.distanceCovered.toString()
        binding.penaltyCheckbox.isChecked = transaction.pentalty
        binding.timeTv.text = transaction.time.toString()

        binding.startLocationLatitudeTv.text = transaction.currentLocationLatitude.toString()
        binding.startLocationLongitudeTv.text = transaction.currentLocationLongitude.toString()
        binding.returnLocationLatitudeTv.text = transaction.returnLocationLatitude.toString()
        binding.returnLocationLongitudeTv.text = transaction.returnLocationLongitude.toString()
        binding.returnTimeTv.text = transaction.returnTime.toString()
        binding.unitPriceTv.text = transaction.priceKm.toString()
        binding.returnPenaltyTv.text = transaction.returnPenalty.toString()
        binding.scooterCommentTv.text = transaction.scooterComment



        loadScooterPhotosLayout = binding.loadScooterPhotosLayout
        loadImagesProgressBar = binding.imageLoadProgressBar

        scooterPhotosGridView = binding.scooterPhotosGrid

        val imgBitmapList: java.util.ArrayList<Bitmap?> = arrayListOf()
        imageAdapter = ImageAdapter(imgBitmapList, this)
        scooterPhotosGridView.adapter = imageAdapter




        val scooterPhotosObjectsList: List<ScooterPhoto> = transaction.scooterPhotos

        val scooterPhotosList: ArrayList<String> = arrayListOf()

        scooterPhotosObjectsList.forEach { scooterPhoto ->
            scooterPhotosList.add(scooterPhoto.imgURL)
        }

        val scooterPhotosArray = arrayOfNulls<String>(scooterPhotosList.size)
        for (i in 0 until scooterPhotosList.size) {
            scooterPhotosArray[i] = scooterPhotosList.get(i)
        }

        val retrieveImageFromUrlAsyncTask = RetrieveImageFromUrlAsyncTask()
        retrieveImageFromUrlAsyncTask.execute(*scooterPhotosArray)

        Handler().postDelayed(Runnable {
            if (!loaded) {
                retrieveImageFromUrlAsyncTask.cancel(true)
                couldNotLoadImages()
            }
        }, 15000L)







        // nickname cannot be null, other public user data can
        binding.transactionUserNicknameTv.text = transaction.userInTransactionNickname
        if (transaction.userInTransactionName == null) {
            binding.transactionUserNameLayout.visibility = View.GONE
        } else {
            binding.transactionUserNameTv.text = transaction.userInTransactionName
        }

        if (transaction.userInTransactionSurname == null) {
            binding.transactionUserSurnameLayout.visibility = View.GONE
        } else {
            binding.transactionUserSurnameTv.text = transaction.userInTransactionSurname
        }

        if (transaction.userInTransactionEmail == null) {
            binding.transactionUserEmailLayout.visibility = View.GONE
        } else {
            binding.transactionUserEmailTv.text = transaction.userInTransactionEmail
        }

        if (transaction.rating == null || transaction.ratingComment == null) {
            binding.existingRatingForTransactionLayout.visibility = View.GONE
            binding.noRatingForTransactionTv.visibility = View.VISIBLE
        } else {
            binding.ratingBar.rating = transaction.rating!!.toFloat()
            binding.ratingCommentTv.text = transaction.ratingComment
        }
    }


    inner class RetrieveImageFromUrlAsyncTask : AsyncTask<String, Int, ArrayList<Bitmap>>() {

        override fun doInBackground(vararg imgUrls: String): ArrayList<Bitmap> {

            var bitmapList: ArrayList<Bitmap> = arrayListOf()
            val count = imgUrls.size

            for (i in 0 until count) {
                var bitmap: Bitmap? = null
                try {
                    val inStream: InputStream = URL(imgUrls[i]).openStream()
                    bitmap = BitmapFactory.decodeStream(inStream)
                    if (bitmap != null)
                        bitmapList.add(bitmap)
                    publishProgress(((i + 1) / count.toFloat() * 100).toInt())
                } catch (ex: IOException) { return arrayListOf() }

            }
            return bitmapList
        }

        override fun onProgressUpdate(vararg values: Int?) {
            loadImagesProgressBar.progress = values[0]!!
            //Toast.makeText(this@TransactionDetailsActivity,"Downloaded ${values[0]} %",Toast.LENGTH_SHORT).show()
            super.onProgressUpdate(values[0])
        }
        override fun onPostExecute(result: ArrayList<Bitmap>) {
            super.onPostExecute(result)
            if (result.isEmpty()) {
                couldNotLoadImages()
            } else {
                imageAdapter.addAll(result)
                loaded = true
            }
        }

    }


    private fun couldNotLoadImages() {
        if (couldNotLoad)
            return

        var couldNotLoadPhotosTv = findViewById<TextView>(R.id.could_not_load_scooter_photos_tv)
        couldNotLoadPhotosTv.visibility = View.VISIBLE
        loadScooterPhotosLayout.visibility = View.GONE
        couldNotLoadPhotosTv.text = "Cannot load scooter images"
        Toast.makeText(this@TransactionDetailsActivity, "Cannot load scooter images", Toast.LENGTH_SHORT)
                .show()

        couldNotLoad = true
    }

}
