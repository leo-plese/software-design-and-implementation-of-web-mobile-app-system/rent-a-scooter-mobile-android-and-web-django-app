package com.seven.iznajmiromobil.view.registeredUser.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.Transaction

class TransactionAdapter(
    var items: List<Transaction>,
    private var clickListener: (Transaction) -> Unit
) :
    RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.transaction_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        fun bind(transaction: Transaction) {
            itemView.findViewById<TextView>(R.id.amount_tv).text = transaction.price.toString()
            itemView.findViewById<TextView>(R.id.pay_person_tv).text =
                "${transaction.userInTransactionName} ${transaction.userInTransactionSurname}"
            val sdf = java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val date = java.util.Date(transaction.returnTime.time)
            itemView.findViewById<TextView>(R.id.time_tv).text = sdf.format(date)
            itemView.rootView.setOnClickListener { clickListener(transaction) }
        }
    }
}