package com.seven.iznajmiromobil.view.registeredUser

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.databinding.UserPublicProfileActivityBinding
import com.seven.iznajmiromobil.models.RatingAndUserInRatingPublicProfile
import com.seven.iznajmiromobil.models.UserPublicProfile
import com.seven.iznajmiromobil.view.base.BaseActivity
import com.seven.iznajmiromobil.view.registeredUser.adapters.RatingAndUserInRatingPublicProfileAdapter
import com.seven.iznajmiromobil.view.registeredUser.mainActivities.commonFunctions.CommonFunctionsClass
import com.seven.iznajmiromobil.view.registeredUser.renter.ReviewClientRequestsActivity
import com.seven.iznajmiromobil.viewmodel.UserViewModel
import javax.inject.Inject

class UserPublicProfileActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: UserPublicProfileActivityBinding

    private lateinit var userViewModel: UserViewModel

    private val SHARED_PREF_NAME = "userSharedPrefs"
    private lateinit var sharedPreferences: SharedPreferences


    // keys (constants - val) in sharedPreferences (map to Boolean value)
    private val IS_CLIENT_KEY: String = "IS_CLIENT"
    private val IS_RENTER_KEY: String = "IS_RENTER"


    private var ratingsAsClientList: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()
    private lateinit var ratingsAsClientAdapter: RatingAndUserInRatingPublicProfileAdapter

    private var ratingsAsRenterList: ArrayList<RatingAndUserInRatingPublicProfile> = arrayListOf()
    private lateinit var ratingsAsRenterAdapter: RatingAndUserInRatingPublicProfileAdapter

    private lateinit var sourceActivity: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.user_public_profile_activity)
        binding.activity = this
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        subscribeUi()

        // important because this activity can be started from many other activities
        // -> according to this extra string determine on back/up btn press where to go to
        sourceActivity = intent.getStringExtra("sourceActivity")

        val userProfileJson = intent.getStringExtra("userProfile")
        val userPublicProfile = Gson().fromJson(userProfileJson, UserPublicProfile::class.java)


        val onRatingListViewItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val ratingAndUserInRatingPublicProfile =
                    parent.getItemAtPosition(position) as RatingAndUserInRatingPublicProfile

                searchUserProfile(ratingAndUserInRatingPublicProfile.userInRatingNickname)
            }

        // 1st ListView - ratings as client
        ratingsAsClientList = userPublicProfile.ratingsAsClient

        val ratingsAsClientListView = binding.ratingsAsClientLayout
        ratingsAsClientAdapter =
            RatingAndUserInRatingPublicProfileAdapter(ratingsAsClientList, this)
        ratingsAsClientListView.adapter = ratingsAsClientAdapter

        ratingsAsClientListView.onItemClickListener = onRatingListViewItemClickListener


        // 2nd ListView - ratings as renter
        ratingsAsRenterList = userPublicProfile.ratingsAsRenter

        val ratingsAsRenterListView = binding.ratingsAsRenterLayout
        ratingsAsRenterAdapter =
            RatingAndUserInRatingPublicProfileAdapter(ratingsAsRenterList, this)
        ratingsAsRenterListView.adapter = ratingsAsRenterAdapter

        ratingsAsRenterListView.onItemClickListener = onRatingListViewItemClickListener



        setUserProfileDetailsUI(userPublicProfile)
    }

    private fun subscribeUi() {

        // when clicked on a list item, display public profile of the user who rated / was rated by the currently displayed user
        userViewModel.getUserPublicProfileResult.observe(this, Observer { userPublicProfile ->
            val userPublicProfileActivityIntent = Intent(this, UserPublicProfileActivity::class.java)
            userPublicProfileActivityIntent.putExtra("userProfile", Gson().toJson(userPublicProfile))
            userPublicProfileActivityIntent.putExtra("sourceActivity", "UserPublicProfileActivity")
            startActivity(userPublicProfileActivityIntent)

            finish()
        })
    }


    private fun searchUserProfile(clickedUserNickname: String) {
        userViewModel.getUserPublicProfile(clickedUserNickname)
    }

    private fun setUserProfileDetailsUI(userPublicProfile: UserPublicProfile) {
        // nickname cannot be null, other public user data can
        binding.userPublicProfileNicknameTv.text = userPublicProfile.nickname
        if (userPublicProfile.name == null) {
            binding.userPublicProfileNameLayout.visibility = View.GONE
        } else {
            binding.userPublicProfileNameTv.text = userPublicProfile.name
        }

        if (userPublicProfile.surname == null) {
            binding.userPublicProfileSurnameLayout.visibility = View.GONE
        } else {
            binding.userPublicProfileSurnameTv.text = userPublicProfile.surname
        }

        if (userPublicProfile.email == null) {
            binding.userPublicProfileEmailLayout.visibility = View.GONE
        } else {
            binding.userPublicProfileEmailTv.text = userPublicProfile.email
        }

        if (ratingsAsClientList.isEmpty()) {
            binding.noRatingsAsClient.visibility = View.VISIBLE
            binding.ratingsAsClientLayout.visibility = View.GONE
        } else {
            // this is by default
            binding.noRatingsAsClient.visibility = View.GONE
            binding.ratingsAsClientLayout.visibility = View.VISIBLE
        }

        if (ratingsAsRenterList.isEmpty()) {
            binding.noRatingsAsRenter.visibility = View.VISIBLE
            binding.ratingsAsRenterLayout.visibility = View.GONE
        } else {
            // this is by default
            binding.noRatingsAsRenter.visibility = View.GONE
            binding.ratingsAsRenterLayout.visibility = View.VISIBLE
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doOnUpAndBackButtonPressed()
        }

        return true
    }

    override fun onBackPressed() {
        doOnUpAndBackButtonPressed()
    }

    private fun doOnUpAndBackButtonPressed() {
        when (sourceActivity) {
            "RatingsActivity" -> startActivity(Intent(this, RatingsActivity::class.java))
            "ReviewClientRequestsActivity" -> startActivity(Intent(this, ReviewClientRequestsActivity::class.java))
            // TODO add all the other special cases (not covered by the default case down below)
            //default case covers calls from the following source activites: "UserPublicProfileActivity", "ClientMainActivity", "RenterMainActivity", "ClientAndRenterMainActivity"
            else -> {
                val IS_CLIENT = sharedPreferences.getBoolean(IS_CLIENT_KEY, false)
                val IS_RENTER = sharedPreferences.getBoolean(IS_RENTER_KEY, false)
                CommonFunctionsClass.startActivityBasedOnRoles(this, IS_CLIENT, IS_RENTER)
            }
        }

        finish()
    }

}
