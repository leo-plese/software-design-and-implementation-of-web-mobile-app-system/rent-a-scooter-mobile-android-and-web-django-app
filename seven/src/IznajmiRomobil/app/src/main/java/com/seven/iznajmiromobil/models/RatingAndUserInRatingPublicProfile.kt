package com.seven.iznajmiromobil.models

class RatingAndUserInRatingPublicProfile {


    // rating data
    var rating: Int = 0
    var comment: String = ""

    // data about user who participated in rating
    var userInRatingNickname: String = ""
    /*
    // see TODO comment in  R.layout.rating_in_user_profile_list_item
    var userInRatingEmail: String? = null
    var userInRatingName: String? = null
    var userInRatingSurname: String? = null
    */

}