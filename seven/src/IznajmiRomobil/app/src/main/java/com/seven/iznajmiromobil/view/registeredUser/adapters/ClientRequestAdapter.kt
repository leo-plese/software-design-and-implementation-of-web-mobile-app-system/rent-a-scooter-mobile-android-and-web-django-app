package com.seven.iznajmiromobil.view.registeredUser.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import com.seven.iznajmiromobil.R
import com.seven.iznajmiromobil.models.ClientRequest
import com.seven.iznajmiromobil.viewmodel.UserViewModel


class ClientRequestAdapter (items: ArrayList<ClientRequest>, context: Context, val userViewModel: UserViewModel) : ArrayAdapter<ClientRequest>(context, R.layout.client_request_list_item, items) {

    var lastSelected = -1

    private class ClientRequestViewHolder {

        internal  var userNicknameTv: TextView? = null
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        internal  var userNameTv: TextView? = null
        internal  var userSurnameTv: TextView? = null
        internal  var userEmailTv: TextView? = null
         */
        internal  var requestTextTv: TextView? = null
        internal  var requestTimeTv: TextView? = null
        internal  var approveRequestBtn: Button? = null

    }
    override fun getView(position: Int, view: View?, viewGroup: ViewGroup): View {

        var view = view
        val viewHolder: ClientRequestViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.client_request_list_item, viewGroup, false)

            viewHolder = ClientRequestViewHolder()

            viewHolder.userNicknameTv = view.findViewById(R.id.user_nickname_tv) as TextView
            /*
            // see TODO comment in  R.layout.rating_in_user_profile_list_item
            viewHolder.userNameTv = view.findViewById(R.id.user_name_tv) as TextView
            viewHolder.userSurnameTv = view.findViewById(R.id.user_surname_tv) as TextView
            viewHolder.userEmailTv = view.findViewById(R.id.user_email_tv) as TextView
             */
            viewHolder.requestTextTv = view.findViewById(R.id.request_text_tv) as TextView
            viewHolder.requestTimeTv = view.findViewById(R.id.request_time_tv) as TextView
            viewHolder.approveRequestBtn = view.findViewById(R.id.approve_request_btn) as Button

        } else {
            //no need to call findViewById, can use existing ones from saved view holder
            viewHolder = view.tag as ClientRequestViewHolder
        }

        val clientRequest = getItem(position)
        viewHolder.userNicknameTv!!.text = clientRequest.clientNickname
        /*
        // see TODO comment in  R.layout.rating_in_user_profile_list_item
        viewHolder.userNameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingName
        viewHolder.userSurnameTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingSurname
        viewHolder.userEmailTv!!.text = ratingAndUserInRatingPublicProfile.userInRatingEmail
        */
        viewHolder.requestTextTv!!.text = clientRequest!!.text
        viewHolder.requestTimeTv!!.text = clientRequest.time.toString()

        //viewHolder.approveRequestBtn!!.isEnabled = (lastSelected == -1)
        viewHolder.approveRequestBtn!!.visibility = if (lastSelected == -1) View.VISIBLE else View.GONE
        viewHolder.approveRequestBtn!!.setOnClickListener {
            showPopupConfirmTransaction(position, clientRequest)

            // Toast.makeText(context, "Clicked button of " + clientRequest.clientNickname, Toast.LENGTH_SHORT).show()
        }
        viewHolder.approveRequestBtn!!.isFocusable = false



        view!!.tag = viewHolder

        return view
    }

    private fun showPopupConfirmTransaction(position: Int, clientRequest: ClientRequest) {
        AlertDialog.Builder(context)
            .setCancelable(false)
            .setTitle(R.string.confirm_request)
            .setMessage(R.string.are_you_sure_confirm_request)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    userViewModel.confirmClientRequestForOffer(clientRequest.chatId, clientRequest.rentOfferId)
                    lastSelected = position
                    notifyDataSetChanged()
                })
            .setNegativeButton(R.string.no,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
            .show()
    }

}