package com.seven.iznajmiromobil.models

import java.sql.Timestamp

class BasicTransaction {

    var ID: Int = 0

    var distanceCovered: Double = 0.0    // random number generated in backend
    var price: Double = 0.0      // backend gets from rent offer data
    var pentalty: Boolean = false  // backend compares current time with returnTime gotten from rent offer data
    var time: Timestamp = Timestamp(System.currentTimeMillis())     // backend writes current time for transaction time
}