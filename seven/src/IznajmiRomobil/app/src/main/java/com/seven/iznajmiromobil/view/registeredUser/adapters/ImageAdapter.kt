package com.seven.iznajmiromobil.view.registeredUser.adapters

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.seven.iznajmiromobil.R

class ImageAdapter(items: ArrayList<Bitmap?>, context: Context) :
    ArrayAdapter<Bitmap?>(context, R.layout.photo_list_item, items) {

    private class ImageViewHolder {
        internal var photoIv: ImageView? = null
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {

        var view = view
        val viewHolder: ImageViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.photo_list_item, viewGroup, false)

            viewHolder = ImageViewHolder()
            viewHolder.photoIv = view.findViewById(R.id.photo_iv) as ImageView

        } else {
            //no need to call findViewById, can use existing ones from saved view holder
            viewHolder = view.tag as ImageViewHolder
        }

        val imageURL = getItem(i)
        viewHolder.photoIv!!.setImageBitmap(imageURL)


        view!!.tag = viewHolder

        return view
    }

}