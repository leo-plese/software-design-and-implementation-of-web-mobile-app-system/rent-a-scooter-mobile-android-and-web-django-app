Rent a Scooter mobile and web app. Mobile app is made for Android platform in Kotlin, web app is implemented in Django framework in Python.

Source code of mobile app is in seven/src/IznajmiRomobil and of web app in seven/src/scooters.

Project documentation is in seven/OPP_2019_Seven_v2_0.pdf, presentation in seven/PrezentacijaProjekta.pptx, project task and product requirements specification in seven/Zadatak.pdf.

Project documentation is made using LaTeX document preparation system (https://www.latex-project.org/) and UML diagrams using Astah software modeling tool (https://astah.net/).

As a team leader, I have not only implemented parts of the apps, but also written team meetings minutes which are located in seven/dnevnik.txt. I have written a significant part of the project documentation, particulary came up with the system design as described in detail textually and graphically using standard UML (Unified Modeling Language, https://www.uml.org/) diagrams as per best practices recommendations in Software Design and Software Engineering. The system design and then implementation as well adhere to Object-oriented programming paradigm standards.

Team project: 7 members.

My role: team leader.

My contribution: designed system architecture, made web and mobile app design, wrote major part of project documentation, implemented part of code, mostly of Android app.

Project duration: Sep 2019 - Feb 2020.
